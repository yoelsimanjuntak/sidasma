<?php $this->load->view('frontend/header') ?>
<div class="col-md-8">
    <!-- Widget: user widget style 1 -->
    <div class="box box-widget widget-user-2">
        <!-- Add the bg color to the header using any of the bg-* classes -->
        <div class="widget-user-header bg-purple-gradient">
            <div class="widget-user-image">
                <img class="img-circle" src="<?=MY_IMAGEURL?>logo.png" style="margin-right: 10px" alt="Logo">
            </div>
            <!-- /.widget-user-image -->
            <h3 class="widget-user-username">Sistem Informasi Dasawisma</h3>
            <h5 class="widget-user-desc">Kabupaten Humbang Hasundutan</h5>
        </div>
        <div class="box-body">
            <h4 class="title">Selamat Datang</h4><hr />
            <p style="text-align: justify">
                <img src="<?=MY_IMAGEURL?>/profil-ketua.jpeg" class="col-sm-3 img-responsive" style="height: 200px; box-shadow: 5px 5px 5px #dedede; padding: 0px; margin-right: 15px" alt="SIDAMA">
                Bangsa Indonesia telah memasuki era globalisasi dan demokrasi yang maju serta desentralisasi yang luas dengan pelaksanaan otonomi daerah yang bertujuan untuk meningkatkan pelayanan guna tercapainya kesejahteraan masyarakat.  Namun demikian kita masih menghadapi berbagai permasalahan pada berbagai aspek. Masalah moral, ekonomi kerakyatan, pendidikan, kesehatan, kekerasan dalam rumah tangga, perdagangan perempuan dan anak dan sebagainya yang semuanya memerlukan prioritas untuk penanganan dan penanggulangannya.  Menjangkau sasaran sebanyak mungkin, maka dibentuk Gerakan Pemberdayaan dan Kesejah-teraan Keluarga, yang mekanisme gerakannya dikelola dan dilaksanakan oleh suatu Tim Penggerak Pemberdayaan dan Kesejahteraan Keluarga (TP.PKK) di setiap jenjang.
            </p>
            <p style="text-align: justify">
                Keberadaan PKK di Kabupaten Humbang Hasundutan dilihat sebagai Gerakan  yang banyak memberikan kontribusi bagi sebagian masyarakat Humbang Hasundutan.
                Hal ini mengandung makna bahwa eksistensi gerakan PKK telah diakui secara luas sebagai gerakan dari dan oleh masyarakat, merupakan mitra kerja pemerintah dalam melaksanakan pembangunan.
                Gerakan PKK sebagai gerakan dari bawah dan bermanfaat bagi masyarakat terutama di daerah pedesaan.
                Selain itu juga turut berperan dalam membangun dunia pendidikan khususnya pada pendidikan Usia Dini (PAUD), Bidang Kesehatan PKK dituntut menghidupkan Posyandu dan membantu membantu ibu yang melahirkan dan akan melahirkan sehingga mengurangi angka kematian ibu dan anak saat proses persalinan, di bidang ekonomi diharapkan PKK dapat membantu usaha kecil menengah.
                Hal terpenting yaitu menyiapkan kader-kader PKK di setiap perkampungan serta dapat melakukan pembinaan kadernya sendiri untuk dapat menyesuaikan sumberdaya sesuai dengan tuntutan dinamika yang berkembang dewasa ini.
            </p>
            <p style="text-align: justify">
                Dalam rangka implementasi Hasil Rakernas VIII PKK Tahun 2015 yang memuat tentang Rencana Kerja Lima Tahun PKK periode 2015-2020 yang dimaksudkan sebagai pedoman, arah kebijakan dan strategi dalam pencapaian keberhasilan 10 (sepuluh) Program Pokok PKK di semua jenjang yang dalam pelaksanaannya disinkronisasikan dengan arah pembangunan di daerah masing-masing.
            </p>
            <p style="text-align: justify">
                Mengingat Program-Program PKK yang dilaksanakan, baik kebijakan umum maupun teknis pelaksanaannya merupakan target pembangunan di segala bidang, maka Tim Penggerak PKK dengan seluruh jajarannya telah bersinergi dengan Pemerintah, Lembaga Sosial Kemasyarakatan dan Dunia Usaha melalui kemitraan yang harmonis untuk mempercepat terlaksananya Program Pembangunan.
            </p>
            <p style="text-align: justify">
                Berdasarkan Hasil keputusan Rapat TP PKK Kabupaten Humbang Hasundutan pada bulan Januari 2018 yaitu dalam penetapan Desa Binaan Tim Penggerak PKK Kabupaten Humbang Hasundutan yang diperlombakan ke Tingkat Provinsi Sumatera Utara Tahun 2018, yakni Pelaksana UP2K PKK, Pelaksana Lingkungan Bersih dan Sehat (LBS), Pelaksana IVA-Test, Pelaksana Pola Asuh Anak dan Remaja (PAAR), Pelaksana Program Terpadu Peningkatan Peranan Wanita Menuju Keluarga Sehat dan Sejahtera (PT P2WKSS).
            </p>
        </div>
        <div class="box-footer no-padding">
            <?php
            $countDesa = $this->db->count_all_results(TBL_MKELURAHAN);
            $countDusun = $this->db->count_all_results(TBL_MDUSUN);
            $countDasawisma = $this->db->count_all_results(TBL_MDASAWISMA);
            $countKK = $this->db->count_all_results(TBL_MKELUARGA);
            ?>
            <ul class="nav nav-stacked">
                <li><a href="#"><b>Desa / Kelurahan</b> <span class="pull-right badge bg-red"><?=$countDesa?></span></a></li>
                <li><a href="#"><b>Dusun</b> <span class="pull-right badge bg-aqua"><?=$countDusun?></span></a></li>
                <li><a href="#"><b>Kelompok Dasawisma</b>  <span class="pull-right badge bg-yellow"><?=$countDasawisma?></span></a></li>
                <li><a href="#"><b>Kepala Keluarga</b>  <span class="pull-right badge bg-green"><?=$countKK?></span></a></li>
            </ul>
        </div>
    </div>

    <div class="box box-default">
        <?php $data = array();
        $i = 0;
        foreach ($res as $d) {
            $res[$i] = array(
                $d[COL_NMKELOMPOK],
                $d[COL_NMKELURAHAN],
                $d[COL_NMDUSUN],
                $d["COUNT_KK"]
            );
            $i++;
        }
        $data = json_encode($res);
        ?>
        <div class="box-header with-border">
            <i class="fa fa-users"></i>
            <h3 class="box-title">Rekapitulasi Kelompok Dasawisma</h3>
        </div>
        <div class="box-body">
            <form id="dataform" method="post" action="#">
                <table id="datalist" class="table table-bordered table-hover">

                </table>
            </form>
        </div>
    </div>
</div>
<?php $this->load->view('frontend/sidebar') ?>
<?php $this->load->view('frontend/footer') ?>
<div class="comment-list" data-count=2>AA</div>
<div class="comment-list" data-count=2>AA</div>
<!--<form>
  <input type="radio" id="type_person" name="type" value="person" checked/>
  <input type="radio" id="type_company" name="type" value="company"/>
  <input type="text" id="first_name" name="first_name" value="John"/>
  <input type="text" id="last_name" name="last_name" value="Doe"/>
  <input type="text" id="email" name="email" value="john@example.com"/>
  <input type="text" id="company_name" name="company_name" value=""/>
  <input type="text" id="phone" name="phone" value="234-567-890"/>
</form>-->
<script>
	var elCount = $('.comment-list');
	var count = elCount.data('count');
	
	for(var i=0; i<elCount.length; i++) {
		$(elCount[i]).html('Loading...');
	}
	
	setTimeout(function(){ 
		$.get( "https://edasawisma.humbanghasundutankab.go.id/ajax/test.jsp?count="+count, function( data ) {
		$('.comment-list').empty();
		var res = JSON.parse(data);
		for(var i=0; i<res.length; i++) {
			var html = '';
			html = '<div class="comment-item"><div class="comment-item__username">'+res[i].username+'</div><div class="comment-item__message">'+res[i].message+'</div></div>';
			$('.comment-list').append(html);
		}
	});
	}, 5000);
	
	
	/*$.get( "https://edasawisma.humbanghasundutankab.go.id/ajax/test.jsp?count="+count, function( data ) {
		$('.comment-list').empty();
		var res = JSON.parse(data);
		for(var i=0; i<res.length; i++) {
			var html = '';
			html = '<div class="comment-item"><div class="comment-item__username">'+res[i].username+'</div><div class="comment-item__message">'+res[i].message+'</div></div>';
			$('.comment-list').append(html);
		}
	});*/
	
	function solution() {
		var form = $('form');
		if($("#type_person", form).is(":checked")) {
			var val1 = $("#first_name", form).val();
			var val2 = $("#last_name", form).val();
			var val3 = $("#email", form).val();
			// re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			var re = /^[a-zA-Z0-9.]*$/;
			if(val1 == '' || val2 == '' || val3 == '') {
				return false;
			}
			
			//return ((re.test(String(val3).toLowerCase()) || val3 == '.@.') && val3.length <= 64);
			var side = val3.split("@");
			if(side.length != 2) return false;
			console.log(side[0].length);
			console.log(side[1].length);
			console.log((side[0].length <= 64) && (side[1].length <= 64));
			return (side[0] != "" && side[1] != "" && re.test(side[0]) && re.test(side[1]) && (side[0].length <= 64 && side[1].length <= 64)); 
		}
		else if($("#type_company", form).is(":checked")) {
			var val1 = $("#company_name", form).val();
			var val2 = $("#phone", form).val();
			if(val1 == '' || val2 == '') {
				return false;
			}
			
			var no = val2.replace(/-/g, "").replace(/ /g, "").replace(/\+/g, "x").replace(/e/g, "x");
			console.log(no);
			console.log(no.length);
			return !isNaN(no) && no.length >= 6;
		}
	}
	
	
    $(document).ready(function() {
        var dataTable = $('#datalist').dataTable({
            //"sDom": "Rlfrtip",
            "aaData": <?=$data?>,
            //"bJQueryUI": true,
            //"aaSorting" : [[5,'desc']],
            "scrollY" : 200,
            "iDisplayLength": 10,
            "aLengthMenu": [[10, 50, 500, -1], [10, 50, 500, "Semua"]],
            "dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
            "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
            "aoColumns": [
                {"sTitle": "Nama Kelompok Dasawisma"},
                {"sTitle": "Kelurahan"},
                {"sTitle": "Dusun"},
                {"sTitle": "Jlh. Keluarga"}
            ]
        });
    });
</script>