<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 04/10/2018
 * Time: 10:35
 */
?>
<?php $this->load->view('frontend/header') ?>
    <div class="col-sm-8">

        <div class="box box-solid">
            <div class="box-header with-border">
                <h4>Lambang PKK</h4>
            </div>
            <div class="box-body">
                <div class="col-sm-12">
                    <p style="text-align: center">
                        <img src="<?=MY_IMAGEURL?>/logopkk.png" class="col-sm-offset-4 col-sm-4 img-responsive" style="box-shadow: 5px 5px 5px #dedede; padding: 0px; margin-right: 15px" alt="SIDAMA">
                    </p>
                    <div class="clearfix"></div>
                    <br />
                    <p style="font-weight: bold; font-style: italic; text-align: center">Sesuai dengan Surat Keputusan Menteri Dalam Negeri Republik Indonesia Nomor 48 Tahun 1983.</p>
                    <ul>
                        <li>
                            <p style="text-decoration: underline">BENTUK</p>
                            <p>Akolade melingkar segi lima dalam arti Pancasila sebagai dasar Gerakan PKK</p>
                            <ol>
                                <li>Bintang</li>
                                <li>17 Butir Akpas, 8 simpul pengikat dan 45 butir padi</li>
                                <li>Akolade melingkar</li>
                                <li>Rangkaian mata rantai</li>
                                <li>Lingkaran putih dengan tulisan Pemberdayaan Kesejahteraan Keluarga, berwarna hitam</li>
                                <li>10 buah ujung tombak yang tersusun merupakan bunga</li>
                            </ol>
                        </li>
                        <br />
                        <li>
                            <p style="text-decoration: underline">WARNA</p>
                            <p>Warna lambang terdiri dari :</p>
                            <ol>
                                <li>Warna dasar lambang adalah biru benhur</li>
                                <li>Warna kuning yang dimaksud adalah warna kuning emas, untuk :
                                    <ul>
                                        <li>Gambar Bintang</li>
                                        <li>Gambar Padi</li>
                                        <li>Gambar Rantai</li>
                                        <li>Gambar Kelopak Bunga Kapas</li>
                                        <li>Gambar Tangkai Padi dan Tangkai Kapas</li>
                                        <li>Gambar Akolade Segi Lima</li>
                                    </ul>
                                </li>
                                <li>Warna putih yang dimaksud adalah Putih Perak untuk :
                                    <ul>
                                        <li>Gambar 10 mata tombak dalam lingkaran paling dalam</li>
                                        <li>Gambar alokade melingkar</li>
                                        <li>Gambar bunga kapas</li>
                                        <li>Delapan simpul pengikat tangkai padi dan kapas</li>
                                    </ul>
                                </li>
                                <li>Putih Kapas untuk :
                                    <ul>
                                        <li>Lingkaran sebagai dasar tulisan Pemberdayaan dan Kesejahteraan Keluarga</li>
                                        <li>Bunga Kapas khusus yang dicetak pada logam</li>
                                    </ul>
                                </li>
                            </ol>
                        </li>
                        <br />
                        <li>
                            <p style="text-decoration: underline">ARTI WARNA</p>
                            <ol>
                                <li>Biru melambangkan suasana damai, aman, tenteram dan sejahtera</li>
                                <li>Putih melambangkan kesucian dan ketulusan untuk satu tujuan dan itikad</li>
                                <li>Kuning melambangkan keagungan dan cita–cita</li>
                                <li>Hitam melambangkan kekekalan/keabadian</li>
                            </ol>
                        </li>
                        <br />
                        <li>
                            <p style="text-decoration: underline">ARTI KOMPONEN</p>
                            <ol style="text-align: justify">
                                <li>Segilima melambangkan Pancasila sebagai dasar Gerakan PKK</li>
                                <li>Bintang melambangkan Ketuhanan Yang Maha Esa</li>
                                <li>17 butir kapas, 8 buah simpul pengikat, 45 butir padi melambangkan kemerdekaan RI dan kemakmuran</li>
                                <li>Akolade melingkar melambangkan wahana partisipasi masyarakat–
                                    masyarakat dalam pembangunan yang memadukan pelaksanaan segala
                                    kegiatan dan prakarsa serta swadaya gotong royong masyarakat dalam
                                    segala aspek kehidupan dan penghidupan untuk mewujudkan Ketahanan
                                    Nasional</li>
                                <li>Rangkaian Mata Rantai melambangkan masyarakat yang terdiri dari
                                    keluarga–keluarga sebagi unit terkecil yang merupakan sasaran Gerakan
                                    PKK</li>
                                <li>Lingkaran Putih melambangkan Pemberdayaan dan Kesejahteraan
                                    Keluarga dilaksanakan secara terus menerus dan berkesinambungan</li>
                                <li>10 buah ujung tombak yang tersusun merupakan bunga melambangkan
                                    gerakan masyarakat dalam pembangunan dengan melaksanakan 10
                                    Program Pokok PKK dan sasarannya keluarga sebagai unit terkecil dalam
                                    masyarakat</li>
                            </ol>
                        </li>
                        <br />
                        <li>
                            <p style="text-decoration: underline">ARTI KESELURUHAN</p>
                            <p style="text-align: justify">
                                Pemberdayaan dan Kesejahteraan Keluarga (PKK) yang merupakan gerakan
                                nasional untuk pembangunan keluarga, berazaskan Pancasila dan UUD 1945 dan
                                bertaqwa kepada Tuhan Yang Maha Esa, melakukan kegiatan yang terus menerus
                                dan berkesinambungan untuk menghimpun, menggerakan dan membina
                                masyarakat dengan melaksanakan 10 Program Pokok PKK dengan sasaran
                                keluarga sebagai unit terkecil dalam masyarakat untuk mewujudkan keluarga
                                sejahtera yang selalu hidup dalam suasana damai, aman, tertib, tenteram, makmur
                                dan sejahtera dalam rangka Ketahanan Nasional.
                            </p>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
<?php $this->load->view('frontend/sidebar') ?>
<?php $this->load->view('frontend/footer') ?>