<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 04/10/2018
 * Time: 11:48
 */
?>
<?php $this->load->view('frontend/header') ?>
    <div class="col-sm-8">

        <div class="box box-solid">
            <div class="box-header with-border">
                <h4>Mars PKK</h4>
            </div>
            <div class="box-body">
                <p style="text-align: center">
                    <img src="<?=MY_IMAGEURL?>/marspkk.jpg" class="col-sm-7 img-responsive" style="box-shadow: 5px 5px 5px #dedede; padding: 0px; margin-right: 15px" alt="SIDAMA">
                </p>
                <p style="font-style: italic; font-size: 10pt">
                    Marilah hai semua rakyat Indonesia...,<br />membangun segra...<br />
                    Membangun kluarga yang sejahtera...,<br />dengan PKK...<br />
                    Hayati dan amalkanlah Pancasila...,<br />untuk negara...<br />
                    Hidup gotong royong makmur pangan dan sandang..,<br />rumah sehat sentosa...<br />
                    Tata laksana di dalam rumahtangga...,<br />rapi dan indah...<br />
                    Didiklah putra berpribadi bangsa...,<br />trampil dan sehat...<br />
                    Kembangkan koprasi jagalah lingkungan dan sekitarnya...<br />
                    Aman dan bahagia kluarga berencana...,<br />hidup jaya pkk
                </p>

            </div>
        </div>
    </div>
<?php $this->load->view('frontend/sidebar') ?>
<?php $this->load->view('frontend/footer') ?>