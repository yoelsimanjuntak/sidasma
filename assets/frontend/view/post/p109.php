<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 04/10/2018
 * Time: 11:48
 */
?>
<?php $this->load->view('frontend/header') ?>
    <div class="col-sm-8">

        <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-purple">
                <div class="widget-user-image">
                    <img class="img-circle" src="<?=MY_IMAGEURL?>/logopkk.png" alt="PKK">
                </div>
                <!-- /.widget-user-image -->
                <h3 class="widget-user-username">Hubungi Kami</h3>
                <h5 class="widget-user-desc"><i class="fa fa-map-marker"></i> JL Letkol P.H Purba , Doloksanggul</h5>
            </div>
            <div class="box-footer no-padding">
                <ul class="nav nav-stacked">
                    <li><a target="_blank" href="mailto:dinas.pmpd2a.hh@gmail.com"><i class="fa fa-envelope"></i>&nbsp;&nbsp;Email <span class="pull-right badge bg-yellow">dinas.pmpd2a.hh@gmail.com</span></a></li>
                    <li><a target="_blank" href="tel:0633-31003"><i class="fa fa-phone"></i>&nbsp;&nbsp;Telepon <span class="pull-right badge bg-aqua">0633-31003</span></a></li>
                    <li><a target="_blank" href="https://web.facebook.com/PKKHumbangHasundutan/"><i class="fa fa-facebook"></i>&nbsp;&nbsp;Facebook <span class="pull-right badge bg-green">PKK Humbang Hasundutan</span></a></li>
                    <li><a target="_blank" href="https://www.instagram.com/"><i class="fa fa-instagram"></i>&nbsp;&nbsp;Instagram <span class="pull-right badge bg-red">PKK Humbang Hasundutan</span></a></li>
                </ul>
            </div>
        </div>
    </div>
<?php $this->load->view('frontend/sidebar') ?>
<?php $this->load->view('frontend/footer') ?>