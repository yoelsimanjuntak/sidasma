<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 04/10/2018
 * Time: 09:45
 */
?>
<?php $this->load->view('frontend/header') ?>
<div class="col-sm-8">
    <div class="box box-solid">
        <div class="box-header with-border">
            <h4>Profil Kabupaten</h4>
        </div>
        <div class="box-body">
            <div class="col-sm-12">
                <ul>
                    <li>
                        <p style="text-decoration: underline">Luas Wilayah dan Geografis</p>
                        <p style="text-align: justify">
                            Luas Kabupaten Humbang Hasundutan adalah 251.765,93 Ha. Terdiri dari 10
                            (sepuluh) Kecamatan, 153 (seratus lima puluh tiga) Desa dan 1 (satu) Kelurahan, yaitu
                            Kecamatan Pakkat, Kecamatan Onanganjang, Kecamatan Sijamapolang, Kecamatan
                            Lintongnihuta, Kecamatan Paranginan, Kecamatan Doloksanggul, Kecamatan Pollung,
                            Kecamatan Parlilitan, Kecamatan Tarabintang dan Kecamatan Baktiraja.
                        </p>
                        <table class="table table-bordered" style="margin-bottom: 5px">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama Kecamatan</th>
                                <th>Luas (Ha)</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>Pakkat</td>
                                <td style="text-align: right">38.168,00</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Onanganjang</td>
                                <td style="text-align: right">22.256,27</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Sijamapolang</td>
                                <td style="text-align: right">14.018,07</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>Lintongnihuta</td>
                                <td style="text-align: right">18.126,03</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>Paranginan</td>
                                <td style="text-align: right">4.778,06</td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td>Doloksanggul</td>
                                <td style="text-align: right">20.929,53</td>
                            </tr>
                            <tr>
                                <td>7</td>
                                <td>Pollung</td>
                                <td style="text-align: right">32.736,46</td>
                            </tr>
                            <tr>
                                <td>8</td>
                                <td>Parlilitan</td>
                                <td style="text-align: right">72.774,71</td>
                            </tr>
                            <tr>
                                <td>9</td>
                                <td>Tarabintang</td>
                                <td style="text-align: right">24.251,98</td>
                            </tr>
                            <tr>
                                <td>10</td>
                                <td>Baktiraja +Luas Danau Toba</td>
                                <td style="text-align: right">2.231,91 + 1.494,91</td>
                            </tr>
                            <tr style="font-weight: bold">
                                <td colspan="2">Humbang Hasundutan</td>
                                <td style="text-align: right">251.765,93</td>
                            </tr>
                            </tbody>
                        </table>
                        <p style="font-style: italic">
                            Sumber : Surat Edaran Bupati Humbang Hasundutan Nomor : 130/1647/Pem/XI/2007, 12
                            November 2007
                        </p>
                        <br />
                        <p>
                            Kabupaten Humbang Hasundutan terletak pada garis 2o1’-2o28’ Lintang Utara dan
                            98o10’-98o58’ Bujur Timur dan berada di bagian tengah wilayah Propinsi Sumatera Utara.
                            Letak Geografis Kabupaten Humbang Hasundutan berbatasan dengan :
                        </p>
                        <p style="line-height: 0.8">1. Sebelah Timur dengan Kabupaten Tapanuli Utara</p>
                        <p style="line-height: 0.8">2. Sebelah Selatan dengan Kabupaten Tapanuli Tengah</p>
                        <p style="line-height: 0.8">3. Sebelah Barat dengan Kabupaten Pakpak Bharat</p>
                        <p style="line-height: 0.8">4. Sebelah Utara dengan Kabupaten Samosir</p>
                    </li>
                </ul>
            </div>

        </div>
    </div>
</div>
<?php $this->load->view('frontend/sidebar') ?>
<?php $this->load->view('frontend/footer') ?>