<?php $this->load->view('header')?>
    <section class="content-header">
        <h1><?= $title ?>  <small>Data</small></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a>
            </li>
            <li class="active">
                Penduduk Pindah
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box box-default">
            <div class="box-body">

                <div class="error-page" style="width: 640px !important;">
                    <h2 class="headline text-red">500</h2>
                    <div class="error-content">
                        <h3><i class="fa fa-cogs text-red"></i>&nbsp;&nbsp;Halaman sedang dalam pengembangan.</h3>
                        <p style="text-align: justify">
                            Halaman / fungsi ini sedang dalam tahap pengembangan. Silakan hubungi Tim Pengembang aplikasi untuk informasi lebih lanjut.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php $this->load->view('loadjs')?>
<?php $this->load->view('footer')?>