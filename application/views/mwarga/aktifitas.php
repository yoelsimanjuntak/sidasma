<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 02/10/2018
 * Time: 19:55
 */
$data = array();
$i = 0;
foreach ($res as $d) {
    $res[$i] = array(
        '<input type="checkbox" class="cekbox" name="cekbox[]" value="' . $d[COL_NIK] . '" />&nbsp;'.anchor('mwarga/aktifitas-edit/'.$d[COL_NIK],"<i class='fa fa-edit'></i>"),
        //anchor('mwarga/kegiatan-edit/'.$d[COL_UNIQ],"Edit"),
        !empty($d[COL_NONIK])?$d[COL_NONIK]:'<p class="text-red" style="font-style: italic">(kosong)</p>',
        strtoupper($d[COL_NMANGGOTA]),
        strtoupper($d[COL_NMKELOMPOK]),
        $d[COL_ISKEG1]?"<i class='fa fa-check text-success'></i>":"<i class='fa fa-times text-danger'></i>",
        $d[COL_ISKEG2]?"<i class='fa fa-check text-success'></i>":"<i class='fa fa-times text-danger'></i>",
        $d[COL_ISKEG3]?"<i class='fa fa-check text-success'></i>":"<i class='fa fa-times text-danger'></i>",
        $d[COL_ISKEG4]?"<i class='fa fa-check text-success'></i>":"<i class='fa fa-times text-danger'></i>",
        $d[COL_ISKEG5]?"<i class='fa fa-check text-success'></i>":"<i class='fa fa-times text-danger'></i>",
        $d[COL_ISKEG6]?"<i class='fa fa-check text-success'></i>":"<i class='fa fa-times text-danger'></i>",
        $d[COL_ISKEG7]?"<i class='fa fa-check text-success'></i>":"<i class='fa fa-times text-danger'></i>"
    );
    $i++;
}
$data = json_encode($res);
?>

<?php $this->load->view('header')
?>
<style>
    .callout-info > div > p {
        margin: 0 0 5px;
    }
</style>
    <section class="content-header">
        <h1><?= $title ?>  <small>Data</small></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a>
            </li>
            <li class="active">
                <?=$title?>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="callout callout-info">
            <h4><i class="fa fa-info-circle"></i> Keterangan :</h4>
            <div class="col-sm-6">
                <p><b>(1)</b> = Penghayatan dan Pengamalan Pancasila.</p>
                <p><b>(2)</b> = Kerja Bakti.</p>
                <p><b>(3)</b> = Rukun Kematian.</p>
                <p><b>(4)</b> = Kegiatan Keagamaan.</p>
            </div>
            <div class="col-sm-6">
                <p><b>(5)</b> = Jumpilitan.</p>
                <p><b>(6)</b> = Arisan.</p>
                <p><b>(7)</b> = Lain-lain.</p>
            </div>
            <div class="clearfix"></div>
        </div>
        <p>
            <?=anchor('mwarga/aktifitas-del','<i class="fa fa-trash-o"></i> Hapus',array('class'=>'cekboxaction btn btn-danger','confirm'=>'Apa anda yakin?'))
            ?>
            <?= anchor('mwarga/aktifitas-add','<i class="fa fa-plus"></i> Data Baru',array('class'=>'btn-add btn btn-primary'))
            ?>
        </p>
        <div class="box box-default">
            <div class="box-body">
                <form id="dataform" method="post" action="#">
                    <table id="datalist" class="table table-bordered table-hover nowrap">

                    </table>
                </form>
            </div>
        </div>
    </section>

    <div class="modal fade" id="detailDialog" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-close"></i></span></button>
                    <h4 class="modal-title">Editor</h4>
                </div>
                <div class="modal-body">
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

<?php $this->load->view('loadjs')?>
    <script type="text/javascript">
        $(document).ready(function() {
            var dataTable = $('#datalist').dataTable({
                //"sDom": "Rlfrtip",
                "aaData": <?=$data?>,
                //"bJQueryUI": true,
                //"aaSorting" : [[5,'desc']],
                "scrollY" : 400,
                "scrollX": "200%",
                "iDisplayLength": 100,
                "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
                "dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
                "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
                "aoColumns": [
                    {"sTitle": "<input type=\"checkbox\" id=\"cekbox\" class=\"\" />", "width": "10px","bSortable":false},
                    //{"sTitle": "#"},
                    {"sTitle": "NIK"},
                    {"sTitle": "Nama"},
                    {"sTitle": "Kel. Dasawisma"},
                    {"sTitle": "(1)","bSortable":false},
                    {"sTitle": "(2)","bSortable":false},
                    {"sTitle": "(3)","bSortable":false},
                    {"sTitle": "(4)","bSortable":false},
                    {"sTitle": "(5)","bSortable":false},
                    {"sTitle": "(6)","bSortable":false},
                    {"sTitle": "(7)","bSortable":false}
                ]
            });
            $('#cekbox').click(function(){
                if($(this).is(':checked')){
                    $('.cekbox').prop('checked',true);
                    console.log('clicked');
                }else{
                    $('.cekbox').prop('checked',false);
                }
            });
        });

        function modalItem(link) {
            var modal = $("#detailDialog").modal("show");
            $(".modal-body", modal).html("<p style='font-style: italic'>Loading...</p>");
            $(".modal-body", modal).load(link, function() {

            });
        }
    </script>

<?php $this->load->view('footer')
?>