<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 01/10/2018
 * Time: 21:47
 */
$this->load->view('header') ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?= $title ?> <small> Form</small></h1>
        <ol class="breadcrumb">
            <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?=site_url('mwarga/meninggal')?>"><?=$title?></a></li>
            <li class="active"><?=$edit?'Edit':'Add'?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-primary" style="border-top-color: transparent">
                    <div class="box-body">
                        <?=form_open(current_url(),array('role'=>'form','id'=>'deviceForm','class'=>'form-horizontal'))?>
                        <div style="display: none" class="alert alert-danger errorBox">
                            <i class="fa fa-ban"></i>
                            <span class="errorMsg"></span>
                        </div>
                        <?php
                        if($this->input->get('success') == 1){
                            ?>
                            <div class="alert alert-success">
                                <i class="fa fa-check"></i>
                                <span class="">Data disimpan</span>
                            </div>
                        <?php
                        }
                        ?>
                        <div class="form-group">
                            <label class="control-label col-sm-2">No. KK</label>
                            <div class="col-sm-5">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="text-kk" value="<?= $edit ? $data[COL_KDKELUARGA]." - ".$data[COL_NMKEPALAKELUARGA] : ""?>" readonly>
                                    <input type="hidden" name="<?=COL_KDKELUARGA?>" value="<?= $edit ? $data[COL_KDKELUARGA] : ""?>" required   >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-bid" data-toggle="modal" data-target="#browseKK" data-toggle="tooltip" data-placement="top" title="Pilih No.KK"><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Kategori</label>
                            <div class="col-sm-5">
                                <select name="<?=COL_KDKATEGORI?>" class="form-control" required>
                                    <?=GetCombobox("SELECT * FROM ".TBL_MKATKOMODITI." ORDER BY ".COL_NMKATEGORI, COL_KDKATEGORI, COL_NMKATEGORI, (!empty($data[COL_KDKATEGORI]) ? $data[COL_KDKATEGORI] : null))?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Nama Komoditas</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="<?= COL_NMKOMODITI?>" value="<?= $edit ? $data[COL_NMKOMODITI] : ""?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Jumlah / Satuan</label>
                            <div class="col-sm-4">
                                <input type="number" placeholder="Jumlah" class="form-control" name="<?= COL_QTY?>" value="<?= $edit ? $data[COL_QTY] : ""?>">
                            </div>
                            <div class="col-sm-4">
                                <input type="text" placeholder="Satuan" class="form-control" name="<?= COL_UOM?>" value="<?= $edit ? $data[COL_UOM] : ""?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12" style="text-align: right">
                                <button type="submit" class="btn btn-primary btn-flat">Simpan</button>
                                <a href="<?=site_url('mwarga/kegiatan/1')?>" class="btn btn-default btn-flat">Kembali ke Daftar&nbsp;&nbsp;<i class="fa fa-arrow-right"></i> </a>
                            </div>

                        </div>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="browseKK" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view('loadjs') ?>
    <script type="text/javascript">
        $("#deviceForm").validate({
            submitHandler : function(form){
                $(form).find('btn').attr('disabled',true);
                $(form).ajaxSubmit({
                    dataType: 'json',
                    type : 'post',
                    success : function(data){
                        $(form).find('btn').attr('disabled',false);
                        if(data.error != 0){
                            $('.errorBox').show().find('.errorMsg').text(data.error);
                        }else{
                            window.location.href = data.redirect;
                        }
                    },
                    error : function(a,b,c){
                        alert('Response Error');
                    }
                });
                return false;
            }
        });

        $('.modal').on('hidden.bs.modal', function (event) {
            $(this).find(".modal-body").empty();
        });

        $('#browseKK').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseKK"));
            $(this).removeData('bs.modal');

            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-kk")?>", function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=KdKeluarga]").val($(this).val());
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=text-kk]").val($(this).val());
                });
            });
        });
    </script>
<?php $this->load->view('footer') ?>