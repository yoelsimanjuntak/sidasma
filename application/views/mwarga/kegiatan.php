<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 01/10/2018
 * Time: 21:33
 */
$data = array();
$i = 0;
foreach ($res as $d) {
    $res[$i] = array(
        '<input type="checkbox" class="cekbox" name="cekbox[]" value="' . $d[COL_UNIQ] . '" />&nbsp;'.anchor('mwarga/kegiatan-edit/'.$d[COL_UNIQ],"<i class='fa fa-edit'></i>"),
        //anchor('mwarga/kegiatan-edit/'.$d[COL_UNIQ],"Edit"),
        !empty($d[COL_NOKK])?$d[COL_NOKK]:'<p class="text-red" style="font-style: italic">(kosong)</p>',
        strtoupper($d[COL_NMKEPALAKELUARGA]),
        strtoupper($d[COL_NMKELOMPOK]),
        $d[COL_NMKATEGORI],
        $d[COL_NMKOMODITI]
    );
    $i++;
}
$data = json_encode($res);
?>

<?php $this->load->view('header')
?>
    <section class="content-header">
        <h1><?= $title ?>  <small>Data</small></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a>
            </li>
            <li class="active">
                <?=$title?>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <p>
            <?=anchor('mwarga/kegiatan-del','<i class="fa fa-trash-o"></i> Hapus',array('class'=>'cekboxaction btn btn-danger','confirm'=>'Apa anda yakin?'))
            ?>
            <?= anchor('mwarga/kegiatan-add/'.$tipe,'<i class="fa fa-plus"></i> Data Baru',array('class'=>'btn-add btn btn-primary'))
            ?>
        </p>
        <div class="box box-default">
            <div class="box-body">
                <form id="dataform" method="post" action="#">
                    <table id="datalist" class="table table-bordered table-hover nowrap">

                    </table>
                </form>
            </div>
        </div>
    </section>

    <div class="modal fade" id="detailDialog" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-close"></i></span></button>
                    <h4 class="modal-title">Editor</h4>
                </div>
                <div class="modal-body">
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

<?php $this->load->view('loadjs')?>
    <script type="text/javascript">
        $(document).ready(function() {
            var dataTable = $('#datalist').dataTable({
                //"sDom": "Rlfrtip",
                "aaData": <?=$data?>,
                //"bJQueryUI": true,
                //"aaSorting" : [[5,'desc']],
                "scrollY" : 400,
                "scrollX": "200%",
                "iDisplayLength": 100,
                "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
                "dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
                "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
                "aoColumns": [
                    {"sTitle": "<input type=\"checkbox\" id=\"cekbox\" class=\"\" />", "width": "10px","bSortable":false},
                    //{"sTitle": "#"},
                    {"sTitle": "No. KK"},
                    {"sTitle": "Kepala Keluarga"},
                    {"sTitle": "Kel. Dasawisma"},
                    {"sTitle": "Kategori"},
                    {"sTitle": "Komoditi"}
                ]
            });
            $('#cekbox').click(function(){
                if($(this).is(':checked')){
                    $('.cekbox').prop('checked',true);
                    console.log('clicked');
                }else{
                    $('.cekbox').prop('checked',false);
                }
            });
        });

        function modalItem(link) {
            var modal = $("#detailDialog").modal("show");
            $(".modal-body", modal).html("<p style='font-style: italic'>Loading...</p>");
            $(".modal-body", modal).load(link, function() {

            });
        }
    </script>

<?php $this->load->view('footer')
?>