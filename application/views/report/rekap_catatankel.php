<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 22/08/2018
 * Time: 00:31
 */
?>
    <head>
        <link rel="stylesheet" href="<?=base_url()?>assets/adminlte/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?=base_url()?>assets/adminlte/dist/css/AdminLTE.min.css">
    </head>
    <style>
        table.with-border {
            border-collapse: collapse;
        }

        table.with-border, th.with-border, td.with-border {
            border: 1px solid black;
        }

        .table-bordered>thead>tr>th {
            text-align: center !important;
        }

        .table>thead:first-child>tr:first-child>th {
            border-top: 0;
        }*/

        .table.no-padding>tbody>tr>td {
            padding: 3px !important;
            border-top: none !important;
        }

        body {
            font-size: 6.5pt !important;
        }

        .text-med {
            font-size: 10pt;
        }

        .text-lg {
            font-size: 12pt;
        }
    </style>
<?php
$n = 0;
foreach($res as $dat) {
    $n++;
    ?>
    <div>
        <div style="text-align: center">
            <p style="font-size: 14px; margin: 0px; text-decoration: underline; font-weight: bold">CATATAN KELUARGA</p>
        </div>

        <div style="margin-top: 15px">
            <table class="table no-padding text-med" style="width: 100%">
                <tr>
                    <td style="width: 19%; padding: 3px">Keluarga</td>
                    <td style="width: 1%">:</td>
                    <td style="width: 30%; text-align: left"><?=$dat[COL_NMKEPALAKELUARGA]?></td>

                    <td style="width: 19%; padding: 3px">Kriteria Rumah</td>
                    <td style="width: 1%">:</td>
                    <td style="width: 30%; text-align: left"><?=$dat[COL_NMKRITERIARUMAH]?></td>
                </tr>
                <tr>
                    <td style="width: 19%; padding: 3px">Kel. Dasawisma</td>
                    <td style="width: 1%">:</td>
                    <td style="width: 30%; text-align: left"><?=$dat[COL_NMKELOMPOK]?></td>

                    <td style="width: 19%; padding: 3px">Jamban Keluarga</td>
                    <td style="width: 1%">:</td>
                    <td style="width: 30%; text-align: left"><?=($dat[COL_ISPUNYAJAMBANKEL]==1?'Ada':'Tidak Ada').($dat[COL_ISPUNYAJAMBANKEL]&&!empty($dat[COL_JLHJAMBANKEL])?' <strong>'.number_format($dat[COL_JLHJAMBANKEL], 0).'</strong> buah':'')?></td>
                </tr>
                <tr>
                    <td style="width: 19%; padding: 3px">Tahun</td>
                    <td style="width: 1%">:</td>
                    <td style="width: 30%; text-align: left"><?=date('Y')?></td>

                    <td style="width: 19%; padding: 3px">Sumber Air</td>
                    <td style="width: 1%">:</td>
                    <td style="width: 30%; text-align: left"><?=!empty($dat[COL_NMSUMBERAIRDESC])?$dat[COL_NMSUMBERAIRDESC]:$dat[COL_NMSUMBERAIR]?></td>
                </tr>
                <tr>
                    <td style="width: 19%; padding: 3px"></td>
                    <td style="width: 1%"></td>
                    <td style="width: 30%; text-align: left"></td>

                    <td style="width: 19%; padding: 3px">Tempat Sampah</td>
                    <td style="width: 1%">:</td>
                    <td style="width: 30%; text-align: left"><?=$dat[COL_ISPUNYAPEMBUANGANSAMPAH]==1?'Ya':'Tidak'?></td>
                </tr>
            </table>
        </div>
        <div class="margin-top: 10px">
            <table class="table table-bordered">
                <thead style="text-align: center">
                <tr>
                    <th style="padding: 5px;text-align: center; width: 10px" rowspan="2">No.</th>
                    <th style="padding: 5px;text-align: center" rowspan="2">Nama Anggota Keluarga</th>
                    <th style="padding: 5px;text-align: center" rowspan="2">Status Perkawinan</th>
                    <th style="padding: 5px;text-align: center" rowspan="2">L/P</th>
                    <th style="padding: 5px;text-align: center" rowspan="2">Tpt. Lahir</th>
                    <th style="padding: 5px;text-align: center" rowspan="2">Tgl/Bln/Thn/Umur</th>
                    <th style="padding: 5px;text-align: center" rowspan="2">Agama</th>
                    <th style="padding: 5px;text-align: center" rowspan="2">Pendidikan</th>
                    <th style="padding: 5px;text-align: center" rowspan="2">Pekerjaan</th>
                    <th style="padding: 5px;text-align: center" rowspan="2">Berkebutuhan Khusus</th>
                    <th style="padding: 5px;text-align: center" colspan="8">Kegiatan PKK yang diikuti</th>
                    <th style="padding: 5px;text-align: center" rowspan="2">Kel. Tani</th>
                </tr>
                <tr style="">
                    <th style="padding: 5px;text-align: center; font-size: 5pt">Penghayatan dan Pengamalan Pancasila</th>
                    <th style="padding: 5px;text-align: center; font-size: 5pt">Gotong Royong</th>
                    <th style="padding: 5px;text-align: center; font-size: 5pt">Pendidikan dan Keterampilan</th>
                    <th style="padding: 5px;text-align: center; font-size: 5pt">Pengembangan Kehidupan Koperasi</th>
                    <th style="padding: 5px;text-align: center; font-size: 5pt">Pangan</th>
                    <th style="padding: 5px;text-align: center; font-size: 5pt">Sandang</th>
                    <th style="padding: 5px;text-align: center; font-size: 5pt">Kesehatan</th>
                    <th style="padding: 5px;text-align: center; font-size: 5pt">Perencanaan Sehat</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $no = 1;
                $anggota = $this->db->where(COL_KDKELUARGA, $dat[COL_KDKELUARGA])->get(TBL_MWARGA)->result_array();
                foreach($anggota as $d) {
                    $diffTL = abs(strtotime(date('d-m-Y')) - strtotime($d[COL_TANGGALLAHIR]));
                    $umur = floor($diffTL / (365*60*60*24));

                    $aktfts = $this->db->where(COL_NIK, $d[COL_NIK])->get(TBL_MAKTIFITAS)->row_array();
                    ?>
                    <tr>
                        <td style="padding: 5px;"><?=$no?></td>
                        <td style="padding: 5px;"><?=$d[COL_NMANGGOTA]?></td>
                        <td style="padding: 5px;"><?=$d[COL_STATUSKAWIN]?></td>
                        <td style="padding: 5px;"><?=$d[COL_JENISKELAMIN]=='Perempuan'?'P':'L'?></td>
                        <td style="padding: 5px;"><?=$d[COL_TEMPATLAHIR]?></td>
                        <td style="padding: 5px;"><?=date('d-m-Y', strtotime($d[COL_TANGGALLAHIR]))?> / <?=$umur?></td>
                        <td style="padding: 5px;"><?=$d[COL_AGAMA]?></td>
                        <td style="padding: 5px;"><?=$d[COL_PENDIDIKAN]?></td>
                        <td style="padding: 5px;"><?=$d[COL_PEKERJAAN]?></td>
                        <td style="padding: 5px;"><?=$d[COL_ISDIFABEL]==1?'Ya':'Tidak'?></td>
                        <td style="padding: 5px;"><?=!empty($aktfts) && $aktfts[COL_ISKEG1]?$aktfts[COL_NMKEG1]:'-'?></td>
                        <td style="padding: 5px;"><?=!empty($aktfts) && $aktfts[COL_ISKEG2]?$aktfts[COL_NMKEG2]:'-'?></td>
                        <td style="padding: 5px;"><?=!empty($aktfts) && $aktfts[COL_ISKEG3]?$aktfts[COL_NMKEG3]:'-'?></td>
                        <td style="padding: 5px;"><?=!empty($aktfts) && $aktfts[COL_ISKEG6]?$aktfts[COL_NMKEG6]:'-'?></td>
                        <td style="padding: 5px;"><?=$dat[COL_NMJENISMAKANANPOKOK]?></td>
                        <td style="padding: 5px;"><?=!empty($aktfts) && $aktfts[COL_ISKEG5]?$aktfts[COL_NMKEG5]:'-'?></td>
                        <td style="padding: 5px;"><?=$d[COL_ISAKTIFPOSYANDU]==1?'Ya':'Tidak'?></td>
                        <td style="padding: 5px;"><?=$d[COL_ISAKSEPTORKB]==1?'Ya':'Tidak'?></td>
                        <td style="padding: 5px;"><?=$dat[COL_ISAKTIFPOKTAN]==1?'Ya':'Tidak'?></td>
                    </tr>
                    <?php
                    $no++;
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php
    if($n != count($res)) {
        ?>
        <pagebreak />
    <?php
    }
}
?>