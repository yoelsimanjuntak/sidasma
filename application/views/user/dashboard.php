<?php $this->load->view('header') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>
<?php
$ruser = GetLoggedUser();
$dusun = $this->db->count_all_results(TBL_MDUSUN);
$dasawisma = $this->db->count_all_results(TBL_MDASAWISMA);
$keluarga = $this->db->count_all_results(TBL_MKELUARGA);
?>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <marquee>
                        <h3 class="box-title text-orange" style="font-weight: bold;">Selamat Datang di Sistem Informasi Dasawisma (SIDAMA) Kabupaten Humbang Hasundutan.</h3>
                    </marquee>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-xs-4">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><?=$dusun?></h3>
                    <p>Dusun</p>
                </div>
                <div class="icon">
                    <i class="fa fa-map-signs"></i>
                </div>
                <a href="<?=site_url("mdusun/index")?>" class="small-box-footer">
                    Tambah Dusun &nbsp;&nbsp;<i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-4 col-xs-4">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3><?=$dasawisma?></h3>
                    <p>Dasawisma</p>
                </div>
                <div class="icon">
                    <i class="fa fa-sitemap"></i>
                </div>
                <a href="<?=site_url("mdasawisma/index")?>" class="small-box-footer">
                    Tambah Dasawisma &nbsp;&nbsp;<i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-4 col-xs-4">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3><?=$keluarga?></h3>
                    <p>Kepala Keluarga</p>
                </div>
                <div class="icon">
                    <i class="fa fa-users"></i>
                </div>
                <a href="<?=site_url("mkeluarga/index")?>" class="small-box-footer">
                    Tambah Kepala Keluarga &nbsp;&nbsp;<i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>

        <div class="col-lg-4 col-xs-4">
            <?php
            $wargaLK = $this->db
                ->where(COL_JENISKELAMIN, "Laki-Laki")
                ->where("EXISTS (select mwargameninggal.NIK from mwargameninggal where mwargameninggal.NIK = mwarga.NIK) = false")
                ->count_all_results(TBL_MWARGA);
            $wargaPR = $this->db
                ->where(COL_JENISKELAMIN, "Perempuan")
                ->where("EXISTS (select mwargameninggal.NIK from mwargameninggal where mwargameninggal.NIK = mwarga.NIK) = false")
                ->count_all_results(TBL_MWARGA);
            ?>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Rasio Jenis Kelamin</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="chart-responsive">
                                <canvas id="chartJenisKelamin" height="155" width="205" style="width: 205px; height: 155px;"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer no-padding">
                    <ul class="nav nav-pills nav-stacked">
                        <li>
                            <a href="javascript: void(0)"><i class="fa fa-male text-red"></i>&nbsp;&nbsp;Laki-Laki
                                <span class="pull-right text-red"><?=number_format(($wargaLK>0?$wargaLK/($wargaLK+$wargaPR):0)*100, 2)?>%</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript: void(0)"><i class="fa fa-female text-light-blue"></i>&nbsp;&nbsp;Perempuan
                                <span class="pull-right text-light-blue"><?=number_format(($wargaPR>0?$wargaPR/($wargaLK+$wargaPR):0)*100, 2)?>%</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-lg-8 col-xs-8">
            <?php
            $dataUmur = $this->db
                ->select("TIMESTAMPDIFF(YEAR,TanggalLahir,CURDATE()) as Umur, COUNT(*) AS Jlh")
                ->where("EXISTS (select mwargameninggal.NIK from mwargameninggal where mwargameninggal.NIK = mwarga.NIK) = false")
                ->group_by("Umur")
                ->get(TBL_MWARGA)->result_array();
            $count0to2 = 0;
            $count2to4 = 0;
            $count5to8 = 0;
            $count9to16 = 0;
            $count17to20 = 0;
            $count21to29 = 0;
            $count30to40 = 0;
            $count41to52 = 0;
            $count53to64 = 0;
            $count64 = 0;
            foreach($dataUmur as $dat) {
                if($dat["Umur"] > 0 && $dat["Umur"] <= 2) $count0to2++;
                else if($dat["Umur"] > 2 && $dat["Umur"] <= 5) $count2to4++;
                else if($dat["Umur"] > 5 && $dat["Umur"] <= 9) $count5to8++;
                else if($dat["Umur"] > 9 && $dat["Umur"] <= 17) $count9to16++;
                else if($dat["Umur"] > 17 && $dat["Umur"] <= 21) $count17to20++;
                else if($dat["Umur"] > 21 && $dat["Umur"] <= 30) $count21to29++;
                else if($dat["Umur"] > 30 && $dat["Umur"] <= 41) $count30to40++;
                else if($dat["Umur"] > 41 && $dat["Umur"] <= 53) $count41to52++;
                else if($dat["Umur"] > 53 && $dat["Umur"] <= 64) $count53to64++;
                else if($dat["Umur"] > 64) $count64++;
            }
            ?>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Rasio Umur</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="chart-responsive">
                                <canvas id="chartUmur" height="155" width="205" style="width: 205px; height: 155px;"></canvas>
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <ul class="nav nav-pills nav-stacked">
                                <li>
                                    <a href="javascript: void(0)" style="color: #ade8f7"><i class="fa fa-angle-right"></i>&nbsp;&nbsp;Umur 0 - 2 th.
                                        <span class="pull-right"><?=number_format($count0to2, 0)?></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript: void(0)" style="color: #00c0ef"><i class="fa fa-angle-right"></i>&nbsp;&nbsp;Umur 2 - 4 th.
                                        <span class="pull-right"><?=number_format($count2to4, 0)?></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript: void(0)" style="color: #229ee6"><i class="fa fa-angle-right"></i>&nbsp;&nbsp;Umur 5 - 8 th.
                                        <span class="pull-right"><?=number_format($count5to8, 0)?></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript: void(0)" style="color: #337ab7"><i class="fa fa-angle-right"></i>&nbsp;&nbsp;Umur 9 - 16 th.
                                        <span class="pull-right"><?=number_format($count9to16, 0)?></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript: void(0)" style="color: #605ca8"><i class="fa fa-angle-right"></i>&nbsp;&nbsp;Umur 17 - 20 th.
                                        <span class="pull-right"><?=number_format($count17to20, 0)?></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript: void(0)" style="color: #00a65a"><i class="fa fa-angle-right"></i>&nbsp;&nbsp;Umur 21 - 29 th.
                                        <span class="pull-right"><?=number_format($count21to29, 0)?></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript: void(0)" style="color: #00a60b"><i class="fa fa-angle-right"></i>&nbsp;&nbsp;Umur 30 - 40 th.
                                        <span class="pull-right"><?=number_format($count30to40, 0)?></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript: void(0)" style="color: #f39c12"><i class="fa fa-angle-right"></i>&nbsp;&nbsp;Umur 41 - 52 th.
                                        <span class="pull-right"><?=number_format($count41to52, 0)?></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript: void(0)" style="color: #dd4b39"><i class="fa fa-angle-right"></i>&nbsp;&nbsp;Umur 53 - 64 th.
                                        <span class="pull-right"><?=number_format($count53to64, 0)?></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript: void(0)" style="color: #777"><i class="fa fa-angle-right"></i>&nbsp;&nbsp;Umur > 64 th.
                                        <span class="pull-right"><?=number_format($count64, 0)?></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="box-footer no-padding">
                    <div class="col-sm-3">

                    </div>
                    <div class="col-sm-3">
                        <ul class="nav nav-pills nav-stacked">

                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <ul class="nav nav-pills nav-stacked">

                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <ul class="nav nav-pills nav-stacked">

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
<?php $this->load->view('loadjs')?>
<script>
    $(document).ready(function() {
        var chartJenisKelaminCanvas = $("#chartJenisKelamin").get(0).getContext("2d");
        var chartJenisKelamin = new Chart(chartJenisKelaminCanvas);
        var JKData = [
            {
                value: <?=$wargaLK?>,
                color: "#f56954",
                highlight: "#f56954",
                label: "Laki-Laki"
            },
            {
                value: <?=$wargaPR?>,
                color: "#00c0ef",
                highlight: "#00c0ef",
                label: "Perempuan"
            }
        ];
        chartJenisKelamin.Doughnut(JKData, {});

        var chartUmurCanvas = $("#chartUmur").get(0).getContext("2d");
        var chartUmur = new Chart(chartUmurCanvas);
        var umurData = [
            {
                value: <?=$count0to2?>,
                color: "#ade8f7",
                highlight: "#ade8f7",
                label: "0-2 thn"
            },
            {
                value: <?=$count2to4?>,
                color: "#00c0ef",
                highlight: "#00c0ef",
                label: "2-4 thn"
            },
            {
                value: <?=$count5to8?>,
                color: "#229ee6",
                highlight: "#229ee6",
                label: "5-8 thn"
            },
            {
                value: <?=$count9to16?>,
                color: "#337ab7",
                highlight: "#337ab7",
                label: "9-16 thn"
            },
            {
                value: <?=$count17to20?>,
                color: "#605ca8",
                highlight: "#605ca8",
                label: "17-20 thn"
            },
            {
                value: <?=$count21to29?>,
                color: "#00a65a",
                highlight: "#00a65a",
                label: "21-29 thn"
            },
            {
                value: <?=$count30to40?>,
                color: "#00a60b",
                highlight: "#00a60b",
                label: "30-40 thn"
            },
            {
                value: <?=$count41to52?>,
                color: "#f39c12",
                highlight: "#f39c12",
                label: "41-52 thn"
            },
            {
                value: <?=$count53to64?>,
                color: "#dd4b39",
                highlight: "#dd4b39",
                label: "53-64 thn"
            },
            {
                value: <?=$count64?>,
                color: "#777",
                highlight: "#777",
                label: "> 64"
            }
        ];
        chartUmur.Doughnut(umurData, {});
    });
</script>
<?php $this->load->view('footer') ?>
