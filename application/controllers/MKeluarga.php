<?php
/**
 * Created by PhpStorm.
 * User: Humbang Hasundutan
 * Date: 8/17/2018
 * Time: 9:48 AM
 */
class Mkeluarga extends MY_Controller {
    function __construct() {
        parent::__construct();
        if(!IsLogin() || (GetLoggedUser()[COL_ROLEID] != ROLEADMIN && GetLoggedUser()[COL_ROLEID] != ROLEUSER)) {
            redirect('user/dashboard');
        }
    }

    function index() {
        $data['title'] = 'Keluarga';
        $this->db->select('*, (SELECT COUNT(*) FROM mwarga w WHERE w.KdKeluarga=mkeluarga.KdKeluarga) AS COUNT_ANGGOTA');
        $this->db->join(TBL_MDASAWISMA,TBL_MDASAWISMA.'.'.COL_KDDASAWISMA." = ".TBL_MKELUARGA.".".COL_KDDASAWISMA,"inner");
        $this->db->join(TBL_MDUSUN,TBL_MDUSUN.'.'.COL_KDDUSUN." = ".TBL_MDASAWISMA.".".COL_KDDUSUN,"inner");
        $this->db->join(TBL_MKELURAHAN,TBL_MKELURAHAN.'.'.COL_KDKELURAHAN." = ".TBL_MDUSUN.".".COL_KDKELURAHAN,"inner");
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            $this->db->where_in(TBL_MKELUARGA.".".COL_KDDASAWISMA, explode(",",GetLoggedUser()[COL_COMPANYID]));
        }
        $this->db->order_by(COL_KDKELUARGA, 'asc');
        $data['res'] = $this->db->get(TBL_MKELUARGA)->result_array();
        $this->load->view('mkeluarga/index', $data);
    }

    function add() {
        $data['title'] = "Keluarga";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mkeluarga/index');
            $data = array(
                COL_KDDASAWISMA => $this->input->post(COL_KDDASAWISMA),
                COL_NOKK => $this->input->post(COL_NOKK),
                COL_NMKEPALAKELUARGA => $this->input->post(COL_NMKEPALAKELUARGA),
                COL_NMMAKANANPOKOK => $this->input->post(COL_NMMAKANANPOKOK),
                COL_NMJENISMAKANANPOKOK => $this->input->post(COL_NMJENISMAKANANPOKOK),
                COL_ISPUNYAJAMBANKEL => $this->input->post(COL_ISPUNYAJAMBANKEL),
                COL_JLHJAMBANKEL => $this->input->post(COL_JLHJAMBANKEL),
                COL_NMSUMBERAIR => $this->input->post(COL_NMSUMBERAIR),
                COL_NMSUMBERAIRDESC => $this->input->post(COL_NMSUMBERAIRDESC),
                COL_NMSUMBERLISTRIK => $this->input->post(COL_NMSUMBERLISTRIK),
                COL_ISPUNYAPEMBUANGANSAMPAH => $this->input->post(COL_ISPUNYAPEMBUANGANSAMPAH),
                COL_ISPUNYAPEMBUANGANLIMBAH => $this->input->post(COL_ISPUNYAPEMBUANGANLIMBAH),
                COL_ISTEMPELSTIKERP4K => $this->input->post(COL_ISTEMPELSTIKERP4K),
                COL_NMKRITERIARUMAH => $this->input->post(COL_NMKRITERIARUMAH),
                COL_ISAKTIFUP2K => $this->input->post(COL_ISAKTIFUP2K),
                COL_NMUP2K => $this->input->post(COL_NMUP2K),
                COL_NMAKTIFITASKESEHATANLING => $this->input->post(COL_NMAKTIFITASKESEHATANLING),
                COL_ISAKTIFPOKTAN => $this->input->post(COL_ISAKTIFPOKTAN),
                COL_ISAKTIFJKN => $this->input->post(COL_ISAKTIFJKN),
                COL_CREATEDBY => $ruser[COL_USERNAME],
                COL_CREATEDDATE => date("Y-m-d")
            );
            if(!$this->db->insert(TBL_MKELUARGA, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->load->view('mkeluarga/form',$data);
        }
    }
    function edit($id) {
        $rdata = $data['data'] = $this->db->where(COL_KDKELUARGA, $id)->get(TBL_MKELUARGA)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }

        $data['title'] = 'Keluarga';
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mkeluarga/index');
            $data = array(
                COL_KDDASAWISMA => $this->input->post(COL_KDDASAWISMA),
                COL_NOKK => $this->input->post(COL_NOKK),
                COL_NMKEPALAKELUARGA => $this->input->post(COL_NMKEPALAKELUARGA),
                COL_NMMAKANANPOKOK => $this->input->post(COL_NMMAKANANPOKOK),
                COL_NMJENISMAKANANPOKOK => $this->input->post(COL_NMJENISMAKANANPOKOK),
                COL_ISPUNYAJAMBANKEL => $this->input->post(COL_ISPUNYAJAMBANKEL),
                COL_JLHJAMBANKEL => $this->input->post(COL_JLHJAMBANKEL),
                COL_NMSUMBERAIR => $this->input->post(COL_NMSUMBERAIR),
                COL_NMSUMBERAIRDESC => $this->input->post(COL_NMSUMBERAIRDESC),
                COL_NMSUMBERLISTRIK => $this->input->post(COL_NMSUMBERLISTRIK),
                COL_ISPUNYAPEMBUANGANSAMPAH => $this->input->post(COL_ISPUNYAPEMBUANGANSAMPAH),
                COL_ISPUNYAPEMBUANGANLIMBAH => $this->input->post(COL_ISPUNYAPEMBUANGANLIMBAH),
                COL_ISTEMPELSTIKERP4K => $this->input->post(COL_ISTEMPELSTIKERP4K),
                COL_NMKRITERIARUMAH => $this->input->post(COL_NMKRITERIARUMAH),
                COL_ISAKTIFUP2K => $this->input->post(COL_ISAKTIFUP2K),
                COL_NMUP2K => $this->input->post(COL_NMUP2K),
                COL_NMAKTIFITASKESEHATANLING => $this->input->post(COL_NMAKTIFITASKESEHATANLING),
                COL_ISAKTIFPOKTAN => $this->input->post(COL_ISAKTIFPOKTAN),
                COL_ISAKTIFJKN => $this->input->post(COL_ISAKTIFJKN),
                COL_MODIFIEDBY => $ruser[COL_USERNAME],
                COL_MODIFIEDDATE => date("Y-m-d")
            );
            if(!$this->db->where(COL_KDKELUARGA, $id)->update(TBL_MKELUARGA, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->load->view('mkeluarga/form',$data);
        }
    }

    function delete(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_MKELUARGA, array(COL_KDKELUARGA => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function addanggota() {
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mkeluarga/edit/'.$this->input->post(COL_KDKELUARGA))."?tab=anggota";
            $data = array(
                COL_KDKELUARGA => $this->input->post(COL_KDKELUARGA),
                COL_NONIK => $this->input->post(COL_NONIK),
                COL_NMANGGOTA => $this->input->post(COL_NMANGGOTA),
                COL_NMJABATAN => $this->input->post(COL_NMJABATAN),
                COL_JENISKELAMIN => $this->input->post(COL_JENISKELAMIN),
                COL_TEMPATLAHIR => $this->input->post(COL_TEMPATLAHIR),
                COL_TANGGALLAHIR => date('Y-m-d', strtotime($this->input->post(COL_TANGGALLAHIR))),
                COL_STATUSKAWIN => $this->input->post(COL_STATUSKAWIN),
                COL_STATUSKELUARGA => $this->input->post(COL_STATUSKELUARGA),
                COL_AGAMA => $this->input->post(COL_AGAMA),
                COL_ALAMAT => $this->input->post(COL_ALAMAT),
                COL_PENDIDIKAN => $this->input->post(COL_PENDIDIKAN),
                COL_PEKERJAAN => $this->input->post(COL_PEKERJAAN),
                COL_ISIVATEST => $this->input->post(COL_ISIVATEST),
                COL_ISAKSEPTORKB => $this->input->post(COL_ISAKSEPTORKB),
                COL_NMAKSEPTORKB => $this->input->post(COL_NMAKSEPTORKB),
                COL_ISAKTIFPOSYANDU => $this->input->post(COL_ISAKTIFPOSYANDU),
                COL_COUNTAKTIFPOSYANDU => $this->input->post(COL_COUNTAKTIFPOSYANDU),
                COL_SATUANAKTIFPOSYANDU => $this->input->post(COL_SATUANAKTIFPOSYANDU),
                COL_ISAKTIFBKB => $this->input->post(COL_ISAKTIFBKB),
                COL_ISPUNYATABUNGAN => $this->input->post(COL_ISPUNYATABUNGAN),
                COL_ISAKTIFKELOMPOKBELAJAR => $this->input->post(COL_ISAKTIFKELOMPOKBELAJAR),
                COL_NMJENISKELOMPOKBELAJAR => $this->input->post(COL_NMJENISKELOMPOKBELAJAR),
                COL_ISAKTIFPAUD => $this->input->post(COL_ISAKTIFPAUD),
                COL_ISAKTIFKOPERASI => $this->input->post(COL_ISAKTIFKOPERASI),
                COL_ISDIFABEL => $this->input->post(COL_ISDIFABEL),
                COL_CREATEDBY => $ruser[COL_USERNAME],
                COL_CREATEDDATE => date("Y-m-d")
            );
            if(!$this->db->insert(TBL_MWARGA, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->load->view('mkeluarga/formkel',$data);
        }
    }

    function editanggota($id) {
        $rdata = $data['data'] = $this->db->where(COL_NIK, $id)->get(TBL_MWARGA)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }

        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mkeluarga/edit/'.$this->input->post(COL_KDKELUARGA))."?tab=anggota";
            $data = array(
                COL_NONIK => $this->input->post(COL_NONIK),
                COL_NMANGGOTA => $this->input->post(COL_NMANGGOTA),
                COL_NMJABATAN => $this->input->post(COL_NMJABATAN),
                COL_JENISKELAMIN => $this->input->post(COL_JENISKELAMIN),
                COL_TEMPATLAHIR => $this->input->post(COL_TEMPATLAHIR),
                COL_TANGGALLAHIR => date('Y-m-d', strtotime($this->input->post(COL_TANGGALLAHIR))),
                COL_STATUSKAWIN => $this->input->post(COL_STATUSKAWIN),
                COL_STATUSKELUARGA => $this->input->post(COL_STATUSKELUARGA),
                COL_AGAMA => $this->input->post(COL_AGAMA),
                COL_ALAMAT => $this->input->post(COL_ALAMAT),
                COL_PENDIDIKAN => $this->input->post(COL_PENDIDIKAN),
                COL_PEKERJAAN => $this->input->post(COL_PEKERJAAN),
                COL_ISIVATEST => $this->input->post(COL_ISIVATEST),
                COL_ISAKSEPTORKB => $this->input->post(COL_ISAKSEPTORKB),
                COL_NMAKSEPTORKB => $this->input->post(COL_NMAKSEPTORKB),
                COL_ISAKTIFPOSYANDU => $this->input->post(COL_ISAKTIFPOSYANDU),
                COL_COUNTAKTIFPOSYANDU => $this->input->post(COL_COUNTAKTIFPOSYANDU),
                COL_SATUANAKTIFPOSYANDU => $this->input->post(COL_SATUANAKTIFPOSYANDU),
                COL_ISAKTIFBKB => $this->input->post(COL_ISAKTIFBKB),
                COL_ISPUNYATABUNGAN => $this->input->post(COL_ISPUNYATABUNGAN),
                COL_ISAKTIFKELOMPOKBELAJAR => $this->input->post(COL_ISAKTIFKELOMPOKBELAJAR),
                COL_NMJENISKELOMPOKBELAJAR => $this->input->post(COL_NMJENISKELOMPOKBELAJAR),
                COL_ISAKTIFPAUD => $this->input->post(COL_ISAKTIFPAUD),
                COL_ISAKTIFKOPERASI => $this->input->post(COL_ISAKTIFKOPERASI),
                COL_ISDIFABEL => $this->input->post(COL_ISDIFABEL),
                COL_MODIFIEDBY => $ruser[COL_USERNAME],
                COL_MODIFIEDDATE => date("Y-m-d")
            );
            if(!$this->db->where(COL_NIK, $id)->where(COL_KDKELUARGA, $rdata[COL_KDKELUARGA])->update(TBL_MWARGA, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->load->view('mkeluarga/formkel',$data);
        }
    }

    function delanggota(){
        $nik = $this->input->get('no');
        $kk = $this->input->get('kk');
        $this->db->delete(TBL_MWARGA, array(COL_KDKELUARGA => $kk, COL_NIK => $nik));
        redirect('mkeluarga/edit/'.$kk);
    }

    function cetak() {
        //load mPDF library
        $this->load->library('Mypdf');
        $pdf = new Mypdf("", "A4-L");

        $data = $this->input->post('cekbox');
        $this->db->select('*, (SELECT COUNT(*) FROM mwarga w WHERE w.KdKeluarga=mkeluarga.KdKeluarga) AS COUNT_ANGGOTA, (SELECT COUNT(*) FROM mwarga w WHERE w.KdKeluarga=mkeluarga.KdKeluarga and w.Jeniskelamin=\'Laki-Laki\') AS COUNT_LK, (SELECT COUNT(*) FROM mwarga w WHERE w.KdKeluarga=mkeluarga.KdKeluarga and w.Jeniskelamin=\'Perempuan\') AS COUNT_PR, (SELECT COUNT(*) FROM mwarga w WHERE w.KdKeluarga=mkeluarga.KdKeluarga and w.StatusKeluarga=\'Suami\') AS COUNT_KK');
        $this->db->join(TBL_MDASAWISMA,TBL_MDASAWISMA.'.'.COL_KDDASAWISMA." = ".TBL_MKELUARGA.".".COL_KDDASAWISMA,"inner");
        $this->db->join(TBL_MDUSUN,TBL_MDUSUN.'.'.COL_KDDUSUN." = ".TBL_MDASAWISMA.".".COL_KDDUSUN,"inner");
        $this->db->join(TBL_MKELURAHAN,TBL_MKELURAHAN.'.'.COL_KDKELURAHAN." = ".TBL_MDUSUN.".".COL_KDKELURAHAN,"inner");
        $this->db->join(TBL_MKECAMATAN,TBL_MKECAMATAN.'.'.COL_KDKECAMATAN." = ".TBL_MKELURAHAN.".".COL_KDKECAMATAN,"inner");
        $this->db->join(TBL_MKABUPATEN,TBL_MKABUPATEN.'.'.COL_KDKABUPATEN." = ".TBL_MKECAMATAN.".".COL_KDKABUPATEN,"inner");
        $this->db->join(TBL_MPROVINSI,TBL_MPROVINSI.'.'.COL_KDPROVINSI." = ".TBL_MKABUPATEN.".".COL_KDPROVINSI,"inner");
        $this->db->where_in(COL_KDKELUARGA, $data);
        $data['res'] = $this->db->get(TBL_MKELUARGA)->result_array();

        //generate the PDF from the given html
        $html = $this->load->view('mkeluarga/cetak',$data,true);
        //echo $html;
        //return;
        $pdf->pdf->SetHTMLFooter("<p style='font-style: italic; font-size: 10px'>Dicetak melalui aplikasi SIDAMA pada ".date("d/m/Y H:i:s")."</p>");
        $pdf->pdf->WriteHTML($html);

        //download it.
        $pdf->pdf->Output("SIDAMA - Kartu Keluarga ".date("d-m-Y").".pdf", "I");
    }
}