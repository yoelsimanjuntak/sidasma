<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 01/10/2018
 * Time: 21:57
 */
class Ajax extends MY_Controller {

    function browse_kk() {
        $this->db->select('*, '.COL_KDKELUARGA.' AS ID, CONCAT('.COL_NMKEPALAKELUARGA.',\' - \','.COL_NOKK.') AS Text');
        $this->db->join(TBL_MDASAWISMA,TBL_MDASAWISMA.'.'.COL_KDDASAWISMA." = ".TBL_MKELUARGA.".".COL_KDDASAWISMA,"inner");
        //$this->db->where(COL_KD_TAHUN_FROM." <=", date("Y"));
        //$this->db->where(COL_KD_TAHUN_TO." >=", date("Y"));
        $ruser = GetLoggedUser();
        if($ruser[COL_ROLEID] != ROLEADMIN) {
            $this->db->where_in(TBL_MKELUARGA.".".COL_KDDASAWISMA, explode(",",$ruser[COL_COMPANYID]));
        }
        $this->db->order_by(COL_KDKELUARGA, 'asc');
        $data['res'] = $this->db->get(TBL_MKELUARGA)->result_array();
        $this->load->view('ajax/browse', $data);
    }

    function browse_nik() {
        $this->db->select('*, '.COL_NIK.' AS ID, CONCAT(UPPER('.COL_NMKELOMPOK.'),\' - \',UPPER('.TBL_MWARGA.".".COL_NONIK.'), \' \', UPPER('.TBL_MWARGA.".".COL_NMANGGOTA.')) AS Text');
        $this->db->join(TBL_MKELUARGA,TBL_MKELUARGA.'.'.COL_KDKELUARGA." = ".TBL_MWARGA.".".COL_KDKELUARGA,"inner");
        $this->db->join(TBL_MDASAWISMA,TBL_MDASAWISMA.'.'.COL_KDDASAWISMA." = ".TBL_MKELUARGA.".".COL_KDDASAWISMA,"inner");
        //$this->db->where(COL_KD_TAHUN_FROM." <=", date("Y"));
        //$this->db->where(COL_KD_TAHUN_TO." >=", date("Y"));
        $ruser = GetLoggedUser();
        if($ruser[COL_ROLEID] != ROLEADMIN) {
            $this->db->where_in(TBL_MKELUARGA.".".COL_KDDASAWISMA, explode(",",$ruser[COL_COMPANYID]));
        }
        $this->db->order_by(TBL_MWARGA.".".COL_NIK, 'asc');
        $data['res'] = $this->db->get(TBL_MWARGA)->result_array();
        $this->load->view('ajax/browse', $data);
    }
	
	function test() {
		header('Access-Control-Allow-Origin: *');
		$count = $this->input->get('count');
		$arr = array();
		for($i=1; $i<=$count; $i++) {
			$arr[] = array(
				'id'=>$i,
				'username'=>'user'.$i,
				'message'=>'Message '.$i
			);
		}
		echo json_encode($arr);
	}
}