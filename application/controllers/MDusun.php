<?php
/**
 * Created by PhpStorm.
 * User: Humbang Hasundutan
 * Date: 8/17/2018
 * Time: 9:48 AM
 */
class Mdusun extends MY_Controller {
    function __construct() {
        parent::__construct();
        if(!IsLogin() || GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }
    }

    function index() {
        $data['title'] = 'Dusun';
        $this->db->select('*, (SELECT COUNT(*) FROM mdasawisma ds WHERE ds.KdDusun=mdusun.KdDusun) AS COUNT_DASAWISMA');
        $this->db->join(TBL_MKELURAHAN,TBL_MKELURAHAN.'.'.COL_KDKELURAHAN." = ".TBL_MDUSUN.".".COL_KDKELURAHAN,"inner");
        $this->db->join(TBL_MKECAMATAN,TBL_MKECAMATAN.'.'.COL_KDKECAMATAN." = ".TBL_MKELURAHAN.".".COL_KDKECAMATAN,"inner");
        $this->db->order_by(COL_NMDUSUN, 'asc');
        $data['res'] = $this->db->get(TBL_MDUSUN)->result_array();
        $this->load->view('mdusun/index', $data);
    }

    function add() {
        $data['title'] = "Dusun";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mdusun/index');
            $data = array(
                COL_KDKELURAHAN => $this->input->post(COL_KDKELURAHAN),
                COL_NMDUSUN => $this->input->post(COL_NMDUSUN),
                COL_NMKEPALADUSUN => $this->input->post(COL_NMKEPALADUSUN),
                COL_CREATEDBY => $ruser[COL_USERNAME],
                COL_CREATEDDATE => date("Y-m-d")
            );
            if(!$this->db->insert(TBL_MDUSUN, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->load->view('mdusun/form',$data);
        }
    }
    function edit($id) {
        $rdata = $data['data'] = $this->db->where(COL_KDDUSUN, $id)->get(TBL_MDUSUN)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }

        $data['title'] = 'Dusun';
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mdusun/index');
            $data = array(
                COL_KDKELURAHAN => $this->input->post(COL_KDKELURAHAN),
                COL_NMDUSUN => $this->input->post(COL_NMDUSUN),
                COL_NMKEPALADUSUN => $this->input->post(COL_NMKEPALADUSUN),
                COL_MODIFIEDBY => $ruser[COL_USERNAME],
                COL_MODIFIEDDATE => date("Y-m-d")
            );
            if(!$this->db->where(COL_KDDUSUN, $id)->update(TBL_MDUSUN, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->load->view('mdusun/form',$data);
        }
    }

    function delete(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_MDUSUN, array(COL_KDDUSUN => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }
}