<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 22/08/2018
 * Time: 20:21
 */
class Mkoperasi extends MY_Controller {
    function __construct() {
        parent::__construct();
        if(!IsLogin() || GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }
    }

    function index() {
        $data['title'] = 'Kategori Koperasi';
        $this->db->order_by(COL_NMJENISKOPERASI, 'asc');
        $data['res'] = $this->db->get(TBL_MJENISKOPERASI)->result_array();
        $this->load->view('mkoperasi/index', $data);
    }

    function add() {
        $data['title'] = "Kategori Koperasi";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mkoperasi/index');
            $data = array(
                COL_NMJENISKOPERASI => $this->input->post(COL_NMJENISKOPERASI),
                COL_CREATEDBY => $ruser[COL_USERNAME],
                COL_CREATEDDATE => date("Y-m-d")
            );
            if(!$this->db->insert(TBL_MJENISKOPERASI, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->load->view('mkoperasi/form',$data);
        }
    }
    function edit($id) {
        $rdata = $data['data'] = $this->db->where(COL_KDJENISKOPERASI, $id)->get(TBL_MJENISKOPERASI)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }

        $data['title'] = 'Kategori Koperasi';
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mkoperasi/index');
            $data = array(
                COL_NMJENISKOPERASI => $this->input->post(COL_NMJENISKOPERASI),
                COL_MODIFIEDBY => $ruser[COL_USERNAME],
                COL_MODIFIEDDATE => date("Y-m-d")
            );
            if(!$this->db->where(COL_KDJENISKOPERASI, $id)->update(TBL_MJENISKOPERASI, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->load->view('mkoperasi/form',$data);
        }
    }

    function delete(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_MJENISKOPERASI, array(COL_KDJENISKOPERASI => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }
}