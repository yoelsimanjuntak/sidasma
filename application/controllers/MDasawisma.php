<?php
/**
 * Created by PhpStorm.
 * User: Humbang Hasundutan
 * Date: 8/17/2018
 * Time: 9:48 AM
 */
class Mdasawisma extends MY_Controller {
    function __construct() {
        parent::__construct();
        if(!IsLogin() || GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }
    }

    function index() {
        $data['title'] = 'Dasawisma';
        $this->db->select('*, (SELECT COUNT(*) FROM mkeluarga kk WHERE kk.KdDasawisma=mdasawisma.KdDasawisma) AS COUNT_KK');
        $this->db->join(TBL_MDUSUN,TBL_MDUSUN.'.'.COL_KDDUSUN." = ".TBL_MDASAWISMA.".".COL_KDDUSUN,"inner");
        $this->db->join(TBL_MKELURAHAN,TBL_MKELURAHAN.'.'.COL_KDKELURAHAN." = ".TBL_MDUSUN.".".COL_KDKELURAHAN,"inner");
        $this->db->order_by(COL_NMKELOMPOK, 'asc');
        $data['res'] = $this->db->get(TBL_MDASAWISMA)->result_array();
        $this->load->view('mdasawisma/index', $data);
    }

    function add() {
        $data['title'] = "Dasawisma";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mdasawisma/index');
            $data = array(
                COL_KDDUSUN => $this->input->post(COL_KDDUSUN),
                COL_NMKELOMPOK => $this->input->post(COL_NMKELOMPOK),
                COL_CREATEDBY => $ruser[COL_USERNAME],
                COL_CREATEDDATE => date("Y-m-d")
            );
            if(!$this->db->insert(TBL_MDASAWISMA, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->load->view('mdasawisma/form',$data);
        }
    }
    function edit($id) {
        $rdata = $data['data'] = $this->db->where(COL_KDDASAWISMA, $id)->get(TBL_MDASAWISMA)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }

        $data['title'] = 'Dasawisma';
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mdasawisma/index');
            $data = array(
                COL_KDDUSUN => $this->input->post(COL_KDDUSUN),
                COL_NMKELOMPOK => $this->input->post(COL_NMKELOMPOK),
                COL_MODIFIEDBY => $ruser[COL_USERNAME],
                COL_MODIFIEDDATE => date("Y-m-d")
            );
            if(!$this->db->where(COL_KDDASAWISMA, $id)->update(TBL_MDASAWISMA, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->load->view('mdasawisma/form',$data);
        }
    }

    function delete(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_MDASAWISMA, array(COL_KDDASAWISMA => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }
}