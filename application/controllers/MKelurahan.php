<?php
/**
 * Created by PhpStorm.
 * User: Humbang Hasundutan
 * Date: 8/17/2018
 * Time: 9:48 AM
 */
class Mkelurahan extends MY_Controller {
    function __construct() {
        parent::__construct();
        if(!IsLogin() || GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }
    }

    function index() {
        $data['title'] = 'Desa / Kelurahan';
        $this->db->select('*, (SELECT COUNT(*) FROM mdusun ds WHERE ds.KdKelurahan=mkelurahan.KdKelurahan) AS COUNT_DS');
        $this->db->join(TBL_MKECAMATAN,TBL_MKECAMATAN.'.'.COL_KDKECAMATAN." = ".TBL_MKELURAHAN.".".COL_KDKECAMATAN,"inner");
        $this->db->join(TBL_MKABUPATEN,TBL_MKABUPATEN.'.'.COL_KDKABUPATEN." = ".TBL_MKECAMATAN.".".COL_KDKABUPATEN,"inner");
        $this->db->order_by(COL_NMKELURAHAN, 'asc');
        $data['res'] = $this->db->get(TBL_MKELURAHAN)->result_array();
        $this->load->view('mkelurahan/index', $data);
    }

    function add() {
        $data['title'] = "Desa / Kelurahan";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mkelurahan/index');
            $data = array(
                COL_KDKECAMATAN => $this->input->post(COL_KDKECAMATAN),
                COL_NMKELURAHAN => $this->input->post(COL_NMKELURAHAN)
            );
            if(!$this->db->insert(TBL_MKELURAHAN, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->load->view('mkelurahan/form',$data);
        }
    }
    function edit($id) {
        $rdata = $data['data'] = $this->db->where(COL_KDKELURAHAN, $id)->get(TBL_MKELURAHAN)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }

        $data['title'] = 'Desa / Kelurahan';
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mkelurahan/index');
            $data = array(
                COL_KDKECAMATAN => $this->input->post(COL_KDKECAMATAN),
                COL_NMKELURAHAN => $this->input->post(COL_NMKELURAHAN)
            );
            if(!$this->db->where(COL_KDKELURAHAN, $id)->update(TBL_MKELURAHAN, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->load->view('mkelurahan/form',$data);
        }
    }

    function delete(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_MKELURAHAN, array(COL_KDKELURAHAN => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }
}