<?php
/**
 * Created by PhpStorm.
 * User: Humbang Hasundutan
 * Date: 8/17/2018
 * Time: 9:48 AM
 */
class Mkomoditi extends MY_Controller {
    function __construct() {
        parent::__construct();
        if(!IsLogin() || GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }
    }

    function index() {
        $data['title'] = 'Kategori Komoditi';
        $this->db->order_by(COL_NMKATEGORI, 'asc');
        $data['res'] = $this->db->get(TBL_MKATKOMODITI)->result_array();
        $this->load->view('mkomoditi/index', $data);
    }

    function add() {
        $data['title'] = "Kategori Komoditi";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mkomoditi/index');
            $data = array(
                COL_NMKATEGORI => $this->input->post(COL_NMKATEGORI),
                COL_CREATEDBY => $ruser[COL_USERNAME],
                COL_CREATEDDATE => date("Y-m-d")
            );
            if(!$this->db->insert(TBL_MKATKOMODITI, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->load->view('mkomoditi/form',$data);
        }
    }
    function edit($id) {
        $rdata = $data['data'] = $this->db->where(COL_KDKATEGORI, $id)->get(TBL_MKATKOMODITI)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }

        $data['title'] = 'Kategori Komoditi';
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mkomoditi/index');
            $data = array(
                COL_NMKATEGORI => $this->input->post(COL_NMKATEGORI),
                COL_MODIFIEDBY => $ruser[COL_USERNAME],
                COL_MODIFIEDDATE => date("Y-m-d")
            );
            if(!$this->db->where(COL_KDKATEGORI, $id)->update(TBL_MKATKOMODITI, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->load->view('mkomoditi/form',$data);
        }
    }

    function delete(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_MKATKOMODITI, array(COL_KDKATEGORI => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }
}