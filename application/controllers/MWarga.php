<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 22/08/2018
 * Time: 12:54
 */
class Mwarga extends MY_Controller {
    function __construct() {
        parent::__construct();
        if(!IsLogin() || (GetLoggedUser()[COL_ROLEID] != ROLEADMIN && GetLoggedUser()[COL_ROLEID] != ROLEUSER)) {
            redirect('user/dashboard');
        }
    }

    function index() {
        $data['title'] = 'Penduduk Aktif';
        $this->db->select('*');
        $this->db->join(TBL_MKELUARGA,TBL_MKELUARGA.'.'.COL_KDKELUARGA." = ".TBL_MWARGA.".".COL_KDKELUARGA,"inner");
        $this->db->join(TBL_MDASAWISMA,TBL_MDASAWISMA.'.'.COL_KDDASAWISMA." = ".TBL_MKELUARGA.".".COL_KDDASAWISMA,"inner");
        $this->db->join(TBL_MDUSUN,TBL_MDUSUN.'.'.COL_KDDUSUN." = ".TBL_MDASAWISMA.".".COL_KDDUSUN,"inner");
        $this->db->join(TBL_MKELURAHAN,TBL_MKELURAHAN.'.'.COL_KDKELURAHAN." = ".TBL_MDUSUN.".".COL_KDKELURAHAN,"inner");
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            $this->db->where_in(TBL_MKELUARGA.".".COL_KDDASAWISMA, explode(",",GetLoggedUser()[COL_COMPANYID]));
        }
        $this->db->where("EXISTS (select mwargameninggal.NIK from mwargameninggal where mwargameninggal.NIK = mwarga.NIK) = false");
        $this->db->order_by(COL_NMANGGOTA, 'asc');
        $data['res'] = $this->db->get(TBL_MWARGA)->result_array();
        $this->load->view('mwarga/index', $data);
    }

    function pindah() {
        $data['title'] = 'Penduduk Pindah';
        $this->load->view('mwarga/pindah', $data);
    }

    function meninggal() {
        $data['title'] = 'Penduduk Meninggal';
        $this->db->join(TBL_MWARGA,TBL_MWARGA.'.'.COL_NIK." = ".TBL_MWARGAMENINGGAL.".".COL_NIK,"inner");
        $this->db->join(TBL_MKELUARGA,TBL_MKELUARGA.'.'.COL_KDKELUARGA." = ".TBL_MWARGA.".".COL_KDKELUARGA,"inner");
        $this->db->join(TBL_MDASAWISMA,TBL_MDASAWISMA.'.'.COL_KDDASAWISMA." = ".TBL_MKELUARGA.".".COL_KDDASAWISMA,"inner");
        $this->db->join(TBL_MDUSUN,TBL_MDUSUN.'.'.COL_KDDUSUN." = ".TBL_MDASAWISMA.".".COL_KDDUSUN,"inner");
        $this->db->join(TBL_MKELURAHAN,TBL_MKELURAHAN.'.'.COL_KDKELURAHAN." = ".TBL_MDUSUN.".".COL_KDKELURAHAN,"inner");
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            $this->db->where_in(TBL_MKELUARGA.".".COL_KDDASAWISMA, explode(",",GetLoggedUser()[COL_COMPANYID]));
        }
        $this->db->order_by(COL_TANGGALKEMATIAN, 'desc');
        $data['res'] = $this->db->get(TBL_MWARGAMENINGGAL)->result_array();
        $this->load->view('mwarga/meninggal', $data);
    }

    function addmeninggal() {
        $data['title'] = "Penduduk Meninggal";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mwarga/meninggal');
            $data = array(
                COL_NIK => $this->input->post(COL_NIK),
                COL_TANGGALKEMATIAN => date("Y-m-d", strtotime($this->input->post(COL_TANGGALKEMATIAN))),
                COL_SEBABKEMATIAN => $this->input->post(COL_SEBABKEMATIAN),
                COL_CREATEDBY => $ruser[COL_USERNAME],
                COL_CREATEDDATE => date("Y-m-d")
            );
            if(!$this->db->insert(TBL_MWARGAMENINGGAL, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->load->view('mwarga/formmeninggal',$data);
        }
    }

    function editmeninggal($id) {
        $rdata = $data['data'] = $this->db->where(COL_NIK, $id)->get(TBL_MWARGAMENINGGAL)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }

        $data['title'] = 'Penduduk Meninggal';
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mwarga/meninggal');
            $data = array(
                COL_TANGGALKEMATIAN => date("Y-m-d", strtotime($this->input->post(COL_TANGGALKEMATIAN))),
                COL_SEBABKEMATIAN => $this->input->post(COL_SEBABKEMATIAN),
                COL_MODIFIEDBY => $ruser[COL_USERNAME],
                COL_MODIFIEDDATE => date("Y-m-d")
            );
            if(!$this->db->where(COL_NIK, $id)->update(TBL_MWARGAMENINGGAL, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->load->view('mwarga/formmeninggal',$data);
        }
    }

    function delmeninggal(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_MWARGAMENINGGAL, array(COL_NIK => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function kegiatan($tipe) {
        $title = "Kegiatan Warga";
        $ruser = GetLoggedUser();
        if($tipe == 1) {
            $title = "Pemanfaatan Tanah / Pekarangan";
        }
        else if($tipe == 2) {
            $title = "Industri Rumah Tangga";
        }
        $data['title'] = $title;
        $data['tipe'] = $tipe;
        $this->db->join(TBL_MKELUARGA,TBL_MKELUARGA.'.'.COL_KDKELUARGA." = ".TBL_MKEGIATAN.".".COL_KDKELUARGA,"inner");
        $this->db->join(TBL_MDASAWISMA,TBL_MDASAWISMA.'.'.COL_KDDASAWISMA." = ".TBL_MKELUARGA.".".COL_KDDASAWISMA,"inner");
        $this->db->join(TBL_MKATKOMODITI,TBL_MKATKOMODITI.'.'.COL_KDKATEGORI." = ".TBL_MKEGIATAN.".".COL_KDKATEGORI,"inner");
        if(!empty($tipe)) {
            $this->db->where(TBL_MKEGIATAN.".".COL_KDTIPE, $tipe);
        }
        if($ruser[COL_ROLEID] != ROLEADMIN) {
            $this->db->where_in(TBL_MKELUARGA.".".COL_KDDASAWISMA, explode(",",$ruser[COL_COMPANYID]));
        }
        $this->db->order_by(TBL_MKEGIATAN.".".COL_KDKELUARGA, 'asc');
        $data['res'] = $this->db->get(TBL_MKEGIATAN)->result_array();
        $this->load->view('mwarga/kegiatan', $data);
    }

    function kegiatan_add($tipe) {
        if($tipe == 1) {
            $title = "Pemanfaatan Tanah / Pekarangan";
        }
        else if($tipe == 2) {
            $title = "Industri Rumah Tangga";
        }
        $data['title'] = $title;
        $data['edit'] = FALSE;
        $data['tipe'] = $tipe;

        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mwarga/kegiatan/'.$tipe);
            $data = array(
                COL_KDKELUARGA => $this->input->post(COL_KDKELUARGA),
                COL_KDTIPE => $tipe,
                COL_KDKATEGORI => $this->input->post(COL_KDKATEGORI),
                COL_NMKOMODITI => $this->input->post(COL_NMKOMODITI),
                COL_QTY => $this->input->post(COL_QTY),
                COL_UOM => $this->input->post(COL_UOM),
                COL_CREATEDBY => $ruser[COL_USERNAME],
                COL_CREATEDDATE => date("Y-m-d")
            );
            if(!$this->db->insert(TBL_MKEGIATAN, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->load->view('mwarga/kegiatan_form',$data);
        }
    }

    function kegiatan_edit($id) {
        $rdata = $data['data'] = $this->db
            ->select(TBL_MKEGIATAN.'.*,'.TBL_MKELUARGA.'.'.COL_NMKEPALAKELUARGA)
            ->join(TBL_MKELUARGA,TBL_MKELUARGA.'.'.COL_KDKELUARGA." = ".TBL_MKEGIATAN.".".COL_KDKELUARGA,"inner")
            ->where(COL_UNIQ, $id)->get(TBL_MKEGIATAN)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }

        if($rdata[COL_KDTIPE] == 1) {
            $title = "Pemanfaatan Tanah / Pekarangan";
        }
        else if($rdata[COL_KDTIPE] == 2) {
            $title = "Industri Rumah Tangga";
        }
        $data['title'] = $title;
        $data['tipe'] = $rdata[COL_KDTIPE];
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mwarga/kegiatan/'.$rdata[COL_KDTIPE]);
            $data = array(
                COL_KDKELUARGA => $this->input->post(COL_KDKELUARGA),
                COL_KDKATEGORI => $this->input->post(COL_KDKATEGORI),
                COL_NMKOMODITI => $this->input->post(COL_NMKOMODITI),
                COL_QTY => $this->input->post(COL_QTY),
                COL_UOM => $this->input->post(COL_UOM),
                COL_MODIFIEDBY => $ruser[COL_USERNAME],
                COL_MODIFIEDDATE => date("Y-m-d")
            );
            if(!$this->db->where(COL_UNIQ, $id)->update(TBL_MKEGIATAN, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->load->view('mwarga/kegiatan_form',$data);
        }
    }

    function kegiatan_del(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_MKEGIATAN, array(COL_UNIQ => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function aktifitas() {
        $title = "Aktifitas Warga";
        $ruser = GetLoggedUser();
        $data['title'] = $title;

        $this->db->join(TBL_MWARGA,TBL_MWARGA.'.'.COL_NIK." = ".TBL_MAKTIFITAS.".".COL_NIK,"inner");
        $this->db->join(TBL_MKELUARGA,TBL_MKELUARGA.'.'.COL_KDKELUARGA." = ".TBL_MWARGA.".".COL_KDKELUARGA,"inner");
        $this->db->join(TBL_MDASAWISMA,TBL_MDASAWISMA.'.'.COL_KDDASAWISMA." = ".TBL_MKELUARGA.".".COL_KDDASAWISMA,"inner");
        if($ruser[COL_ROLEID] != ROLEADMIN) {
            $this->db->where_in(TBL_MKELUARGA.".".COL_KDDASAWISMA, explode(",",$ruser[COL_COMPANYID]));
        }
        $this->db->order_by(TBL_MAKTIFITAS.".".COL_NIK, 'asc');
        $data['res'] = $this->db->get(TBL_MAKTIFITAS)->result_array();
        $this->load->view('mwarga/aktifitas', $data);
    }

    function aktifitas_add() {
        $data['edit'] = FALSE;
        $data['title'] = 'Aktifitas Warga';

        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mwarga/aktifitas');
            $nik = $this->input->post(COL_NIK);
            $data = array(
                COL_NIK => (!empty($nik) ? $nik : null),
                COL_ISKEG1 => $this->input->post(COL_ISKEG1),
                COL_NMKEG1 => $this->input->post(COL_NMKEG1),
                COL_ISKEG2 => $this->input->post(COL_ISKEG2),
                COL_NMKEG2 => $this->input->post(COL_NMKEG2),
                COL_ISKEG3 => $this->input->post(COL_ISKEG3),
                COL_NMKEG3 => $this->input->post(COL_NMKEG3),
                COL_ISKEG4 => $this->input->post(COL_ISKEG4),
                COL_NMKEG4 => $this->input->post(COL_NMKEG4),
                COL_ISKEG5 => $this->input->post(COL_ISKEG5),
                COL_NMKEG5 => $this->input->post(COL_NMKEG5),
                COL_ISKEG6 => $this->input->post(COL_ISKEG6),
                COL_NMKEG6 => $this->input->post(COL_NMKEG6),
                COL_ISKEG7 => $this->input->post(COL_ISKEG7),
                COL_NMKEG7 => $this->input->post(COL_NMKEG7),
                COL_CREATEDBY => $ruser[COL_USERNAME],
                COL_CREATEDDATE => date("Y-m-d")
            );
            if(!$this->db->insert(TBL_MAKTIFITAS, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->load->view('mwarga/aktifitas_form',$data);
        }
    }

    function aktifitas_edit($id) {
        $rdata = $data['data'] = $this->db
            ->select(TBL_MAKTIFITAS.'.*,'.TBL_MWARGA.'.'.COL_NMANGGOTA)
            ->join(TBL_MWARGA,TBL_MWARGA.'.'.COL_NIK." = ".TBL_MAKTIFITAS.".".COL_NIK,"inner")
            ->where(TBL_MWARGA.'.'.COL_NIK, $id)->get(TBL_MAKTIFITAS)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }
        $data['title'] = 'Aktifitas Warga';
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mwarga/aktifitas');
            $data = array(
                COL_NIK => (!empty($nik) ? $nik : null),
                COL_ISKEG1 => $this->input->post(COL_ISKEG1),
                COL_NMKEG1 => $this->input->post(COL_NMKEG1),
                COL_ISKEG2 => $this->input->post(COL_ISKEG2),
                COL_NMKEG2 => $this->input->post(COL_NMKEG2),
                COL_ISKEG3 => $this->input->post(COL_ISKEG3),
                COL_NMKEG3 => $this->input->post(COL_NMKEG3),
                COL_ISKEG4 => $this->input->post(COL_ISKEG4),
                COL_NMKEG4 => $this->input->post(COL_NMKEG4),
                COL_ISKEG5 => $this->input->post(COL_ISKEG5),
                COL_NMKEG5 => $this->input->post(COL_NMKEG5),
                COL_ISKEG6 => $this->input->post(COL_ISKEG6),
                COL_NMKEG6 => $this->input->post(COL_NMKEG6),
                COL_ISKEG7 => $this->input->post(COL_ISKEG7),
                COL_NMKEG7 => $this->input->post(COL_NMKEG7),
                COL_MODIFIEDBY => $ruser[COL_USERNAME],
                COL_MODIFIEDDATE => date("Y-m-d")
            );
            if(!$this->db->where(COL_UNIQ, $id)->update(TBL_MWARGA, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->load->view('mwarga/aktifitas_form',$data);
        }
    }

    function aktifitas_del(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_MAKTIFITAS, array(COL_NIK => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }
}