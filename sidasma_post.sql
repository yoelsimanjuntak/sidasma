/*
SQLyog Community v12.09 (64 bit)
MySQL - 10.1.22-MariaDB : Database - sidasma
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sidasma` /*!40100 DEFAULT CHARACTER SET latin1 */;

/*Table structure for table `postcategories` */

DROP TABLE IF EXISTS `postcategories`;

CREATE TABLE `postcategories` (
  `PostCategoryID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryName` varchar(50) NOT NULL,
  PRIMARY KEY (`PostCategoryID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `postcategories` */

insert  into `postcategories`(`PostCategoryID`,`PostCategoryName`) values (1,'News'),(2,'Blog'),(3,'Event'),(4,'Gallery'),(5,'Others');

/*Table structure for table `posts` */

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `PostID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryID` int(10) NOT NULL,
  `PostDate` date NOT NULL,
  `PostTitle` varchar(200) NOT NULL,
  `PostSlug` varchar(200) NOT NULL,
  `PostContent` longtext NOT NULL,
  `PostExpiredDate` date NOT NULL,
  `TotalView` int(11) NOT NULL DEFAULT '0',
  `LastViewDate` datetime DEFAULT NULL,
  `IsSuspend` tinyint(1) NOT NULL DEFAULT '1',
  `FileName` varchar(250) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) NOT NULL,
  `UpdatedOn` datetime NOT NULL,
  PRIMARY KEY (`PostID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `posts` */

insert  into `posts`(`PostID`,`PostCategoryID`,`PostDate`,`PostTitle`,`PostSlug`,`PostContent`,`PostExpiredDate`,`TotalView`,`LastViewDate`,`IsSuspend`,`FileName`,`CreatedBy`,`CreatedOn`,`UpdatedBy`,`UpdatedOn`) values (1,1,'2018-10-15','Interdum et malesuada fames ac ante ipsum elementum vel lorem eu primis in faucibus','interdum-et-malesuada-fames-ac-ante-ipsum-elementum-vel-lorem-eu-primis-in-faucibus','<ul>\r\n	<li>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</li>\r\n	<li>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</li>\r\n</ul>\r\n','2020-01-01',12,NULL,0,'jan-ethes-dan-presiden-jokowi_20181007_090059.jpg','admin','2016-10-18 19:14:58','admin','2018-10-15 15:05:51'),(2,1,'2016-11-10','Swimmer Ryan Lochte wins endorsement deal for ‘forgiving’ throat drop','swimmer-ryan-lochte-wins-endorsement-deal-for-forgiving-throat-drop','<p>Integer rhoncus vestibulum lectus ac sodales. Vestibulum dapibus, magna quis finibus scelerisque, sapien nisl rutrum mauris, eget ullamcorper est purus pharetra orci. Nam eu magna sit amet dui convallis rhoncus non vel odio. Phasellus in libero nec nunc venenatis finibus sed in neque.Nullam auctor, quam ut eleifend hendrerit, tellus mauris dignissim nibh, nec euismod felis odio ac ex. Maecenas finibus lacinia fermentum.Vestibulum hendrerit, mauris vel convallis semper, ante purus vehicula diam, non blandit leo leo ac justo. Praesent viverra, lacus et tempus tincidunt, massa eros eleifend massa, ac tempus ipsum justo vel erat.Mauris vitae magna lacinia, vehicula diam sed, rutrum tortor. Maecenas orci nibh, tincidunt quis eros ac, vehicula rhoncus lacus. Maecenas ut justo sit amet lectus consequat mollis. Phasellus vehicula consequat vehicula.<em>&quot;Fusce nulla turpis, tempor at auctor et, dignissim semper ligula. Cras eu dolor blandit, facilisis mi et, ultrices orci sapien nisl rutrum mauris, eget ullamcorper est purus pharetra orci. Nam eu magna sit amet dui convallis rhoncus non vel odio. Phasellus in libero nec nunc venenatis finibus sed in neque.</em>Vestibulum hendrerit, mauris vel convallis semper, ante purus vehicula diam, non blandit leo leo ac justo. Praesent viverra, lacus et tempus tincidunt, massa eros eleifend massa, ac tempus ipsum justo vel erat.</p>\r\n','2020-01-01',25,NULL,0,NULL,'admin','2016-10-18 20:16:10','admin','2016-11-10 02:36:42'),(3,3,'2016-11-10','U.S. Navy ship fires warning shots at Iranian vessel','u-s-navy-ship-fires-warning-shots-at-iranian-vessel','<ul>\r\n	<li>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</li>\r\n	<li>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</li>\r\n</ul>\r\n','2020-01-01',3,NULL,0,NULL,'admin','2016-10-18 20:25:16','admin','2016-11-10 02:36:31'),(4,5,'2016-10-22','Contact Us','contact-us','<p>DEL dimaknai sebagai `Pemimpin yang Selangkah Lebih Maju`.</p>\r\n\r\n<p>Yayasan Del mulai aktif di kegiatan sosial kemasyarakatan jauh sebelum didirikannya PT Toba Sejahtera, perusahaan yang kemudian menjadi Yayasan Del sebagai tonggak tanggung jawab sosial perusahaan.</p>\r\n\r\n<p>Tujuan awal Yayasan Del didirikan adalah untuk memberikan akses pendidikan berkualitas di daerah terpencil bagi siswa/i berprestasi dengan latar belakang ekonomi yang kurang menguntungkan, yaitu dengan mendirikan Politeknik Informatika Del dan SMA Unggul Del di Laguboti, Sumatera Utara serta Sekolah NOAH di Kalisari, Jakarta Timur.</p>\r\n\r\n<p>Tidak hanya berkiprah di bidang pendidikan, Yayasan Del juga aktif bekerjasama dengan pemerintah daerah dan lembaga sosial yang ada di Indonesia untuk meningkatkan pelayanan serta memperluas cakrawala bidang pelayanan strategis lainnya.</p>\r\n\r\n<p>Politeknik Informatika Del didirikan pada tahun 2001 dan bertujuan untuk menyediakan pendidikan tinggi berkualitas internasional, bagi siswa/i berpotensi, terutama dengan latar belakang ekonomi yang kurang menguntungkan, khususnya yang berasal dari Sumatera Utara.</p>\r\n\r\n<p>Disamping turut berperan sebagai inisiator penggerak pembangunan di Tapanuli, IT Del juga diharapkan dapat menginkubasi lahirnya para&nbsp;<em>technopreneur</em>&nbsp;baru di bidang teknologi informasi.</p>\r\n\r\n<p>Dengan lokasi di daerah tepian Danau Toba, berjarak sekitar 400 KM dari kota Medan, area IT Del diharapkan dapat memberikan suasana tenang dan kondusif dalam belajar, karena jauh dari kebisingan dan hiruk pikuk suasana perkotaan/perindustrian.</p>\r\n','2020-01-01',0,NULL,0,NULL,'admin','2016-10-22 20:23:03','admin','2016-10-22 20:23:03'),(5,5,'2016-10-22','FAQ','faq','<blockquote>\r\n<ol>\r\n	<li><strong>Saya mahasiswa / alumni IT DEL, apakah saya perlu mendaftar sebagai member Del Career Center?</strong></li>\r\n</ol>\r\n</blockquote>\r\n\r\n<blockquote>\r\n<ul>\r\n	<li>Setiap mahasiswa dan alumni IT DEL&nbsp;secara otomatis terdaftar sebagai member Del Career Center. Untuk informasi lebih lanjut, silahkan hubungi Administrator</li>\r\n</ul>\r\n</blockquote>\r\n','2020-01-01',5,NULL,0,NULL,'admin','2016-10-22 20:26:38','admin','2016-10-22 20:29:34'),(6,4,'2018-10-15','Foto 1','foto-1','<p>Test Gallery</p>\r\n','2020-01-01',1,NULL,0,'jan-ethes-dan-presiden-jokowi_20181007_0900591.jpg','admin','2018-10-15 15:45:16','admin','2018-10-15 15:45:16'),(7,4,'2018-10-15','Foto 2','foto-2','<p>Optimus!</p>\r\n','2020-01-01',12,NULL,0,'autobots_logo_by_theseventhshadow.jpg','admin','2018-10-15 15:59:20','admin','2018-10-15 15:59:20');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
