/*
SQLyog Community v12.09 (64 bit)
MySQL - 10.1.22-MariaDB : Database - sidasma
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`hh_dasawisma` /*!40100 DEFAULT CHARACTER SET latin1 */;

/*Table structure for table `language` */

DROP TABLE IF EXISTS `language`;

CREATE TABLE `language` (
  `LanguageID` int(11) NOT NULL AUTO_INCREMENT,
  `LanguageName` varchar(200) NOT NULL,
  PRIMARY KEY (`LanguageID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `language` */

/*Table structure for table `maktifitas` */

DROP TABLE IF EXISTS `maktifitas`;

CREATE TABLE `maktifitas` (
  `NIK` varchar(200) NOT NULL,
  `IsKeg1` tinyint(1) DEFAULT NULL,
  `NmKeg1` varchar(200) DEFAULT NULL,
  `IsKeg2` tinyint(1) DEFAULT NULL,
  `NmKeg2` varchar(200) DEFAULT NULL,
  `IsKeg3` tinyint(1) DEFAULT NULL,
  `NmKeg3` varchar(200) DEFAULT NULL,
  `IsKeg4` tinyint(1) DEFAULT NULL,
  `NmKeg4` varchar(200) DEFAULT NULL,
  `IsKeg5` tinyint(1) DEFAULT NULL,
  `NmKeg5` varchar(200) DEFAULT NULL,
  `IsKeg6` tinyint(1) DEFAULT NULL,
  `NmKeg6` varchar(200) DEFAULT NULL,
  `IsKeg7` tinyint(1) DEFAULT NULL,
  `NmKeg7` varchar(200) DEFAULT NULL,
  `CreatedBy` varchar(200) NOT NULL,
  `CreatedDate` date NOT NULL,
  `ModifiedBy` varchar(200) DEFAULT NULL,
  `ModifiedDate` date DEFAULT NULL,
  PRIMARY KEY (`NIK`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `maktifitas` */

insert  into `maktifitas`(`NIK`,`IsKeg1`,`NmKeg1`,`IsKeg2`,`NmKeg2`,`IsKeg3`,`NmKeg3`,`IsKeg4`,`NmKeg4`,`IsKeg5`,`NmKeg5`,`IsKeg6`,`NmKeg6`,`IsKeg7`,`NmKeg7`,`CreatedBy`,`CreatedDate`,`ModifiedBy`,`ModifiedDate`) values ('1212071711930001',1,'OK',1,'YA',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','admin','2018-10-03',NULL,NULL);

/*Table structure for table `mdasawisma` */

DROP TABLE IF EXISTS `mdasawisma`;

CREATE TABLE `mdasawisma` (
  `KdDasawisma` bigint(10) NOT NULL AUTO_INCREMENT,
  `KdDusun` bigint(10) NOT NULL,
  `NmKelompok` varchar(250) NOT NULL,
  `CreatedBy` varchar(200) NOT NULL,
  `CreatedDate` date NOT NULL,
  `ModifiedBy` varchar(200) DEFAULT NULL,
  `ModifiedDate` date DEFAULT NULL,
  PRIMARY KEY (`KdDasawisma`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `mdasawisma` */

insert  into `mdasawisma`(`KdDasawisma`,`KdDusun`,`NmKelompok`,`CreatedBy`,`CreatedDate`,`ModifiedBy`,`ModifiedDate`) values (1,8,'MELATI 1','','0000-00-00',NULL,NULL),(2,8,'MELATI 2','','0000-00-00',NULL,NULL),(3,8,'MELATI 3','','0000-00-00',NULL,NULL),(4,8,'MELATI 4','','0000-00-00',NULL,NULL),(5,8,'MELATI 5','','0000-00-00',NULL,NULL);

/*Table structure for table `mdusun` */

DROP TABLE IF EXISTS `mdusun`;

CREATE TABLE `mdusun` (
  `KdDusun` bigint(10) NOT NULL AUTO_INCREMENT,
  `KdKelurahan` bigint(10) NOT NULL,
  `NmDusun` varchar(250) NOT NULL,
  `NmKepalaDusun` varchar(250) NOT NULL,
  `CreatedBy` varchar(200) NOT NULL,
  `CreatedDate` date NOT NULL,
  `ModifiedBy` varchar(200) DEFAULT NULL,
  `ModifiedDate` date DEFAULT NULL,
  PRIMARY KEY (`KdDusun`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `mdusun` */

insert  into `mdusun`(`KdDusun`,`KdKelurahan`,`NmDusun`,`NmKepalaDusun`,`CreatedBy`,`CreatedDate`,`ModifiedBy`,`ModifiedDate`) values (1,147,'ANGGOCCI','-','','0000-00-00',NULL,NULL),(2,147,'LAEMAGA','-','','0000-00-00',NULL,NULL),(3,147,'RAMBUNG','-','','0000-00-00',NULL,NULL),(4,147,'SIANTAR-SITANDUK','-','','0000-00-00',NULL,NULL),(5,147,'RUMBIA','-','','0000-00-00',NULL,NULL),(6,147,'KARONTANG','-','','0000-00-00',NULL,NULL),(8,147,'SIMBARA','-','','0000-00-00',NULL,NULL),(9,147,'NAPA CINGKAM','-','','0000-00-00',NULL,NULL);

/*Table structure for table `mjeniskoperasi` */

DROP TABLE IF EXISTS `mjeniskoperasi`;

CREATE TABLE `mjeniskoperasi` (
  `KdJenisKoperasi` bigint(10) NOT NULL AUTO_INCREMENT,
  `NmJenisKoperasi` varchar(200) NOT NULL,
  `CreatedBy` varchar(200) NOT NULL,
  `CreatedDate` date NOT NULL,
  `ModifiedBy` varchar(200) DEFAULT NULL,
  `ModifiedDate` date DEFAULT NULL,
  PRIMARY KEY (`KdJenisKoperasi`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `mjeniskoperasi` */

insert  into `mjeniskoperasi`(`KdJenisKoperasi`,`NmJenisKoperasi`,`CreatedBy`,`CreatedDate`,`ModifiedBy`,`ModifiedDate`) values (1,'Simpan Pinjam','admin','2018-08-22',NULL,NULL),(2,'Usaha Bersama','admin','2018-08-22','admin','2018-08-22');

/*Table structure for table `mkabupaten` */

DROP TABLE IF EXISTS `mkabupaten`;

CREATE TABLE `mkabupaten` (
  `KdKabupaten` bigint(10) NOT NULL AUTO_INCREMENT,
  `KdProvinsi` bigint(10) NOT NULL,
  `NmKabupaten` varchar(250) NOT NULL,
  PRIMARY KEY (`KdKabupaten`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `mkabupaten` */

insert  into `mkabupaten`(`KdKabupaten`,`KdProvinsi`,`NmKabupaten`) values (1,1,'HUMBANG HASUNDUTAN');

/*Table structure for table `mkatbacaan` */

DROP TABLE IF EXISTS `mkatbacaan`;

CREATE TABLE `mkatbacaan` (
  `KdKategori` bigint(10) NOT NULL AUTO_INCREMENT,
  `NmKategori` varchar(200) NOT NULL,
  `CreatedBy` varchar(200) NOT NULL,
  `CreatedDate` date NOT NULL,
  `ModifiedBy` varchar(200) DEFAULT NULL,
  `ModifiedDate` date DEFAULT NULL,
  PRIMARY KEY (`KdKategori`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `mkatbacaan` */

insert  into `mkatbacaan`(`KdKategori`,`NmKategori`,`CreatedBy`,`CreatedDate`,`ModifiedBy`,`ModifiedDate`) values (1,'Pertanian','admin','2018-08-22',NULL,NULL),(2,'Pendidikan','admin','2018-08-22',NULL,NULL),(3,'Keterampilan','admin','2018-08-22',NULL,NULL);

/*Table structure for table `mkatkomoditi` */

DROP TABLE IF EXISTS `mkatkomoditi`;

CREATE TABLE `mkatkomoditi` (
  `KdKategori` bigint(10) NOT NULL AUTO_INCREMENT,
  `NmKategori` varchar(200) NOT NULL,
  `CreatedBy` varchar(200) DEFAULT NULL,
  `CreatedDate` date NOT NULL,
  `ModifiedBy` varchar(200) NOT NULL,
  `ModifiedDate` date DEFAULT NULL,
  PRIMARY KEY (`KdKategori`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `mkatkomoditi` */

insert  into `mkatkomoditi`(`KdKategori`,`NmKategori`,`CreatedBy`,`CreatedDate`,`ModifiedBy`,`ModifiedDate`) values (2,'Palawija','admin','2018-08-22','admin','2018-08-22'),(3,'Makanan Pokok','admin','2018-08-22','',NULL),(4,'Ternak Hidup & Daging','admin','2018-08-22','',NULL),(5,'Bahan Bakar','admin','2018-08-22','',NULL),(6,'Logam','admin','2018-08-22','',NULL),(7,'Konveksi','admin','2018-10-01','',NULL);

/*Table structure for table `mkecamatan` */

DROP TABLE IF EXISTS `mkecamatan`;

CREATE TABLE `mkecamatan` (
  `KdKecamatan` bigint(10) NOT NULL AUTO_INCREMENT,
  `KdKabupaten` bigint(10) NOT NULL,
  `NmKecamatan` varchar(250) NOT NULL,
  PRIMARY KEY (`KdKecamatan`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `mkecamatan` */

insert  into `mkecamatan`(`KdKecamatan`,`KdKabupaten`,`NmKecamatan`) values (1,1,'PARLILITAN'),(2,1,'POLLUNG'),(3,1,'BAKTIRAJA'),(4,1,'PARANGINAN'),(5,1,'LINTONGNIHUTA'),(6,1,'DOLOKSANGGUL'),(7,1,'SIJAMAPOLANG'),(8,1,'ONAN GANJANG'),(9,1,'PAKKAT'),(10,1,'TARABINTANG');

/*Table structure for table `mkegiatan` */

DROP TABLE IF EXISTS `mkegiatan`;

CREATE TABLE `mkegiatan` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `KdKeluarga` varchar(200) NOT NULL,
  `KdTipe` varchar(200) NOT NULL,
  `KdKategori` varchar(200) NOT NULL,
  `NmKomoditi` varchar(200) NOT NULL,
  `Qty` int(10) NOT NULL,
  `UOM` varchar(200) NOT NULL,
  `CreatedBy` varchar(200) NOT NULL,
  `CreatedDate` date NOT NULL,
  `ModifiedBy` varchar(200) DEFAULT NULL,
  `ModifiedDate` date DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `mkegiatan` */

insert  into `mkegiatan`(`Uniq`,`KdKeluarga`,`KdTipe`,`KdKategori`,`NmKomoditi`,`Qty`,`UOM`,`CreatedBy`,`CreatedDate`,`ModifiedBy`,`ModifiedDate`) values (1,'1981212100909999','1','4','Ayam',100,'ekor','admin','2018-10-01','admin','2018-10-01'),(2,'1272070104080230','2','7','KEBAYA',100,'potong','admin','2018-10-02',NULL,NULL);

/*Table structure for table `mkeluarga` */

DROP TABLE IF EXISTS `mkeluarga`;

CREATE TABLE `mkeluarga` (
  `KdKeluarga` varchar(250) NOT NULL,
  `KdDasawisma` bigint(10) NOT NULL,
  `NmKepalaKeluarga` varchar(250) NOT NULL,
  `NmMakananPokok` varchar(250) DEFAULT NULL,
  `NmJenisMakananPokok` varchar(250) DEFAULT NULL,
  `IsPunyaJambanKel` tinyint(1) DEFAULT NULL,
  `JlhJambanKel` bigint(10) DEFAULT NULL,
  `NmSumberAir` varchar(250) DEFAULT NULL,
  `NmSumberAirDesc` varchar(250) DEFAULT NULL,
  `IsPunyaPembuanganSampah` tinyint(1) DEFAULT NULL,
  `IsPunyaPembuanganLimbah` tinyint(1) DEFAULT NULL,
  `IsTempelStikerP4K` tinyint(1) DEFAULT NULL,
  `NmKriteriaRumah` varchar(250) DEFAULT NULL,
  `IsAktifUP2K` tinyint(1) DEFAULT NULL,
  `NmUP2K` varchar(250) DEFAULT NULL,
  `NmAktifitasKesehatanLing` varchar(250) DEFAULT NULL,
  `CreatedBy` varchar(200) NOT NULL,
  `CreatedDate` date NOT NULL,
  `ModifiedBy` varchar(200) DEFAULT NULL,
  `ModifiedDate` date DEFAULT NULL,
  PRIMARY KEY (`KdKeluarga`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mkeluarga` */

insert  into `mkeluarga`(`KdKeluarga`,`KdDasawisma`,`NmKepalaKeluarga`,`NmMakananPokok`,`NmJenisMakananPokok`,`IsPunyaJambanKel`,`JlhJambanKel`,`NmSumberAir`,`NmSumberAirDesc`,`IsPunyaPembuanganSampah`,`IsPunyaPembuanganLimbah`,`IsTempelStikerP4K`,`NmKriteriaRumah`,`IsAktifUP2K`,`NmUP2K`,`NmAktifitasKesehatanLing`,`CreatedBy`,`CreatedDate`,`ModifiedBy`,`ModifiedDate`) values ('1212020204080112',1,'JOKO WIDODO','Beras','RAMOS CAP MAWAR',1,3,'DLL','SUNGAI',1,1,0,'Sehat',1,'USAHA','Tidak','','0000-00-00','admin','2018-10-03'),('1212070104080110',1,'LAMBOK MANIK','Beras','Beras',0,1,'pdam',NULL,0,0,0,'Sehat',NULL,'Aktifitas','Tidak','','0000-00-00','admin','2018-09-06'),('1213070202080111',3,'PRABOWO','Non Beras','Jagung',0,0,'meminta-minta',NULL,0,0,0,'Kurang Sehat',NULL,'non','Tidak','','0000-00-00','admin','2018-09-06'),('1272070104080110',2,'JORBUT',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','0000-00-00',NULL,NULL),('1272070104080230',4,'PASTAP MARULAP-ULAP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','0000-00-00',NULL,NULL),('1981212100909999',5,'Yoel Rolas Simanjuntak','Beras','Beras',1,1,'PDAM',NULL,1,1,0,'Sehat',NULL,'Aktifitas','Ya','admin','2018-09-06',NULL,NULL);

/*Table structure for table `mkelurahan` */

DROP TABLE IF EXISTS `mkelurahan`;

CREATE TABLE `mkelurahan` (
  `KdKelurahan` bigint(10) NOT NULL AUTO_INCREMENT,
  `KdKecamatan` bigint(10) NOT NULL,
  `NmKelurahan` varchar(250) NOT NULL,
  PRIMARY KEY (`KdKelurahan`)
) ENGINE=InnoDB AUTO_INCREMENT=156 DEFAULT CHARSET=latin1;

/*Data for the table `mkelurahan` */

insert  into `mkelurahan`(`KdKelurahan`,`KdKecamatan`,`NmKelurahan`) values (1,1,'SIHOTANG HASUGIAN DOLOK I'),(2,1,'PUSUK II SIMANINGGIR'),(3,1,'SIONOM HUDON VII'),(4,1,'BARINGIN'),(5,1,'SIONOM HUDON JULU'),(6,1,'SIONOM HUDON TONGA'),(7,1,'SIONOM HUDON UTARA'),(8,1,'SIONOM HUDON SELATAN'),(9,1,'SIONOM HUDON TIMUR'),(10,1,'SIHOTANG HASUGIAN TONGA'),(11,1,'PUSUK I'),(12,1,'SIONOM HUDON TORUAN'),(13,1,'SIMATANIARI'),(14,1,'SIONOM HUDON TIMUR II'),(15,1,'SIHOTANG HASUGIAN DOLOK II'),(16,1,'SIHOTANG HASUGIAN HABINSARAN'),(17,1,'SIONOM HUDON SIBULBULON'),(18,1,'SIONOM HUDON RUNGGU'),(19,1,'JANJI HUTANAPA'),(20,1,'BARINGIN NATAM'),(21,2,'HUTAPAUNG'),(22,2,'POLLUNG'),(23,2,'HUTAJULU'),(24,2,'RIARIA'),(25,2,'PARSINGGURAN II'),(26,2,'PANSURBATU'),(27,2,'AEK NAULI I'),(28,2,'AEK NAULI II'),(29,2,'PANDUMAAN'),(30,2,'SIPITUHUTA'),(31,2,'PARDOMUAN'),(32,2,'HUTAPAUNG UTARA'),(33,2,'PARSINGGURAN I'),(34,3,'MARBUN TORUAN'),(35,3,'SIMAMORA'),(36,3,'TIPANG'),(37,3,'SINAMBELA'),(38,3,'SIMANGULAMPE'),(39,3,'SIUNONGUNONG JULU'),(40,3,'MARBUN TONGA'),(41,4,'PARANGINAN UTARA'),(42,4,'LUMBAN SIALAMAN'),(43,4,'PEARUNG'),(44,4,'LOBUTOLONG'),(45,4,'LUMBAN BARAT'),(46,4,'SIBORUTOROP'),(47,4,'PEARUNG SILALI'),(48,4,'LUMBAN SIANTURI'),(49,4,'LOBUTOLONG HABINSARAN'),(50,4,'SIHONONGAN'),(51,4,'PARANGINAN SELATAN'),(52,5,'SIGOMPUL'),(53,5,'BONANDOLOK'),(54,5,'SIGUMPAR'),(55,5,'PARULOHAN'),(56,5,'SITIO II'),(57,5,'HUTASOIT II'),(58,5,'NAGASARIBU III'),(59,5,'NAGASARIBU IV'),(60,5,'NAGASARIBU V'),(61,5,'SIBUNTUON PARTUR'),(62,5,'NAGASARIBU II'),(63,5,'SIHARJULU'),(64,5,'SITOLUBAHAL'),(65,5,'TAPIAN NAULI'),(66,5,'PARGAULAN'),(67,5,'LOBUTUA'),(68,5,'DOLOK MARGU'),(69,5,'HUTASOIT I'),(70,5,'NAGASARIBU I'),(71,5,'SIBUNTUON PARPEA'),(72,5,'SIBUNTUON PARPEA'),(73,5,'SIPONJOT'),(74,5,'HABEAHAN'),(75,6,'HUTABAGASAN'),(76,6,'MATITI II'),(77,6,'SAITNIHUTA'),(78,6,'HUTARAJA'),(79,6,'LUMBAN PURBA'),(80,6,'BONANIONAN'),(81,6,'PARIKSINOMBA'),(82,6,'SIHITE II'),(83,6,'JANJI'),(84,6,'PAKKAT'),(85,6,'SIRISIRISI'),(86,6,'PASAR DOLOKSANGGUL'),(87,6,'HUTAGURGUR'),(88,6,'SILEANG'),(89,6,'MATITI I'),(90,6,'PURBA MANALU'),(91,6,'AEK LUNG'),(92,6,'PASARIBU'),(93,6,'SIMANGARONSANG'),(94,6,'SAMPEAN'),(95,6,'SOSORTOLONG SIHITE III'),(96,6,'SILAGALAGA'),(97,6,'SIMARIGUNG'),(98,6,'SOSOR GONTING'),(99,6,'SOSOR TAMBOK'),(100,6,'PURBA DOLOK'),(101,6,'SIHITE I'),(102,6,'LUMBAN TOBING'),(103,7,'HUTA GINJANG'),(104,7,'SIBORBORON'),(105,7,'BONANDOLOK I'),(106,7,'BONANDOLOK II'),(107,7,'SIBUNTUON'),(108,7,'NAGURGURAN'),(109,7,'BATUNAJAGAR'),(110,7,'SANGGARAN I'),(111,7,'SITAPONGAN'),(112,7,'SIGULOK'),(113,8,'ONAN GANJANG'),(114,8,'BATUNAGODANG SIATAS'),(115,8,'PARBOTIHAN'),(116,8,'SIHIKKIT'),(117,8,'PARNAPA'),(118,8,'JANJI NAGODANG'),(119,8,'SAMPETUA'),(120,8,'HUTAJULU'),(121,8,'SIBULUAN'),(122,8,'SIGALOGO'),(123,8,'AEK GODANG ARBAAN'),(124,8,'SANGGARAN II'),(125,9,'SIJARANGO'),(126,9,'TUKKA DOLOK'),(127,9,'SIAMBATON'),(128,9,'SIPAGABU'),(129,9,'RURA TANJUNG'),(130,9,'RURA AEKSOPANG'),(131,9,'LUMBAN TONGATONGA'),(132,9,'PULO GODANG'),(133,9,'PAKKAT HAUAGONG'),(134,9,'PEADUNGDUNG'),(135,9,'SIJARANGO I'),(136,9,'AMBOBI PARANGINAN'),(137,9,'PURBA SIANJUR'),(138,9,'SIAMBATON PAHAE'),(139,9,'PANGGUGUNAN'),(140,9,'HAUAGONG'),(141,9,'BANUAREA'),(142,9,'PARMONANGAN'),(143,9,'PURBA BERSATU'),(144,9,'PURBA BARINGIN'),(145,9,'KARYA'),(146,9,'MARBUN'),(147,10,'SITANDUK'),(148,10,'SIHOMBU'),(149,10,'SIMBARA'),(150,10,'MARPADAN'),(151,10,'MUNGKUR'),(152,10,'SIBONGKARE SIANJU'),(153,10,'TARABINTANG'),(154,10,'SIHOTANG HASUGIAN TORUAN'),(155,10,'SIBONGKARE');

/*Table structure for table `mprovinsi` */

DROP TABLE IF EXISTS `mprovinsi`;

CREATE TABLE `mprovinsi` (
  `KdProvinsi` bigint(10) NOT NULL AUTO_INCREMENT,
  `NmProvinsi` varchar(250) NOT NULL,
  PRIMARY KEY (`KdProvinsi`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `mprovinsi` */

insert  into `mprovinsi`(`KdProvinsi`,`NmProvinsi`) values (1,'SUMATERA UTARA');

/*Table structure for table `mstatuskel` */

DROP TABLE IF EXISTS `mstatuskel`;

CREATE TABLE `mstatuskel` (
  `KdStatus` varchar(200) NOT NULL,
  `Seq` int(10) NOT NULL,
  PRIMARY KEY (`KdStatus`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mstatuskel` */

insert  into `mstatuskel`(`KdStatus`,`Seq`) values ('Anak',3),('Istri',2),('Suami',1);

/*Table structure for table `mwarga` */

DROP TABLE IF EXISTS `mwarga`;

CREATE TABLE `mwarga` (
  `NIK` varchar(250) NOT NULL,
  `KdKeluarga` varchar(250) NOT NULL,
  `NmAnggota` varchar(250) NOT NULL,
  `NmJabatan` varchar(250) DEFAULT NULL,
  `JenisKelamin` varchar(250) DEFAULT NULL,
  `TempatLahir` varchar(250) DEFAULT NULL,
  `TanggalLahir` date DEFAULT NULL,
  `StatusKawin` varchar(250) DEFAULT NULL,
  `StatusKeluarga` varchar(250) DEFAULT NULL,
  `Agama` varchar(250) DEFAULT NULL,
  `Alamat` varchar(250) DEFAULT NULL,
  `KdKelurahan` bigint(10) DEFAULT NULL,
  `StatusTinggal` varchar(250) DEFAULT NULL,
  `Pendidikan` varchar(250) DEFAULT NULL,
  `Pekerjaan` varchar(250) DEFAULT NULL,
  `IsAkseptorKB` tinyint(1) DEFAULT NULL,
  `NmAkseptorKB` varchar(250) DEFAULT NULL,
  `IsAktifPosyandu` tinyint(1) DEFAULT NULL,
  `CountAktifPosyandu` int(10) DEFAULT NULL,
  `SatuanAktifPosyandu` varchar(200) DEFAULT NULL,
  `IsAktifBKB` tinyint(1) DEFAULT NULL,
  `IsPunyaTabungan` tinyint(1) DEFAULT NULL,
  `IsAktifKelompokBelajar` tinyint(1) DEFAULT NULL,
  `NmJenisKelompokBelajar` varchar(250) DEFAULT NULL,
  `IsAktifPAUD` tinyint(1) DEFAULT NULL,
  `IsAktifKoperasi` tinyint(1) DEFAULT NULL,
  `NmJenisKoperasi` varchar(250) DEFAULT NULL,
  `IsDifabel` tinyint(1) DEFAULT NULL,
  `NmDifabel` varchar(250) DEFAULT NULL,
  `CreatedBy` varchar(200) NOT NULL,
  `CreatedDate` date NOT NULL,
  `ModifiedBy` varchar(200) DEFAULT NULL,
  `ModifiedDate` date DEFAULT NULL,
  PRIMARY KEY (`NIK`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mwarga` */

insert  into `mwarga`(`NIK`,`KdKeluarga`,`NmAnggota`,`NmJabatan`,`JenisKelamin`,`TempatLahir`,`TanggalLahir`,`StatusKawin`,`StatusKeluarga`,`Agama`,`Alamat`,`KdKelurahan`,`StatusTinggal`,`Pendidikan`,`Pekerjaan`,`IsAkseptorKB`,`NmAkseptorKB`,`IsAktifPosyandu`,`CountAktifPosyandu`,`SatuanAktifPosyandu`,`IsAktifBKB`,`IsPunyaTabungan`,`IsAktifKelompokBelajar`,`NmJenisKelompokBelajar`,`IsAktifPAUD`,`IsAktifKoperasi`,`NmJenisKoperasi`,`IsDifabel`,`NmDifabel`,`CreatedBy`,`CreatedDate`,`ModifiedBy`,`ModifiedDate`) values ('1111111','1212020204080112','JOKO WIDODO','SEKRETARIS','Laki-Laki','Solo','1968-08-12','Menikah','Suami','Islam','SOLO',NULL,NULL,'S1','Wirausaha',0,NULL,0,3,'Bulan',0,1,1,'PAKET B',0,1,NULL,0,NULL,'','0000-00-00','admin','2018-10-01'),('1212071711930001','1212070104080110','Riris Manik',NULL,'Perempuan','Porsea','1993-11-17','Lajang','Anak','Katolik','TPL',NULL,NULL,'S1','Wirausaha',0,NULL,0,NULL,NULL,0,1,1,NULL,0,0,NULL,0,NULL,'admin','2018-09-06',NULL,NULL),('12120909090192190','1212070104080110','Lambok Manik',NULL,'Laki-Laki','Repa','1967-10-31','Menikah','Suami','Katolik','tpl',NULL,NULL,'SMU/Sederajat','Swasta',0,NULL,0,NULL,NULL,0,0,0,NULL,0,1,NULL,0,NULL,'admin','2018-09-06',NULL,NULL),('12120987880987876','1981212100909999','Miguel',NULL,'Laki-Laki','Medan','1990-12-13','Menikah','Istri','Katolik','PT Toba Pulp Lestari',NULL,NULL,'Diploma','Wirausaha',1,NULL,1,NULL,NULL,1,1,1,NULL,1,1,NULL,0,NULL,'admin','2018-09-06',NULL,NULL),('12121099090909','1213070202080111','Prabowo',NULL,'Laki-Laki','sds','1979-06-05','Menikah','Suami','Buddha','ds',NULL,NULL,'Tidak tamat SD','Petani',0,NULL,0,NULL,NULL,0,0,0,NULL,0,0,NULL,1,NULL,'admin','2018-09-06',NULL,NULL),('1212121121213333','1981212100909999','Riris Manik',NULL,'Perempuan','Porsea ','1993-11-17','Lajang','Anak','Katolik','PT TPL Porsea',NULL,NULL,'S1','Petani',0,NULL,0,NULL,NULL,0,NULL,0,NULL,0,0,NULL,0,NULL,'admin','2018-09-06',NULL,NULL),('1212190909098980','1212070104080110','Rosita Sinaga',NULL,'Perempuan','Sitinggitinggi','1969-09-21','Menikah','Istri','Katolik','tlp',NULL,NULL,'SMU/Sederajat','Petani',0,NULL,0,NULL,NULL,0,1,1,NULL,0,1,NULL,0,NULL,'admin','2018-09-06',NULL,NULL),('12129098989898989','1981212100909999','Yoel Rolas Simanjuntak',NULL,'Laki-Laki','Medan','1979-05-28','Menikah','Suami','Katolik','PT TPL',NULL,NULL,'S1','Petani',0,NULL,0,NULL,NULL,0,0,0,NULL,0,0,NULL,0,NULL,'admin','2018-09-06',NULL,NULL),('2222222','1212020204080112','IRIANA JOKO WIDODO',NULL,'Perempuan','Solo','1969-08-12','Menikah','Istri','Islam','IRIANA JOKO WIDODO',NULL,NULL,'S1','Ibu Rumah Tangga',1,NULL,1,NULL,NULL,1,1,0,NULL,0,1,NULL,0,NULL,'','0000-00-00',NULL,NULL),('3333333','1212020204080112','GIBRAN RAKABUMING',NULL,'Laki-Laki','Solo','1987-08-12','Menikah','Anak','Islam','SOLO',NULL,NULL,'S1','Wirausaha',0,NULL,0,NULL,NULL,0,1,0,NULL,0,0,NULL,0,NULL,'','0000-00-00',NULL,NULL),('4444444','1212020204080112','KAHIYANG AYU',NULL,'Perempuan','Solo','1988-08-12','Menikah','Anak','Islam','SOLO',NULL,NULL,'S1','Wirausaha',0,NULL,1,NULL,NULL,1,1,0,NULL,0,0,NULL,0,NULL,'','0000-00-00',NULL,NULL),('5555555','1212020204080112','KAESANG PANGAREP',NULL,'Laki-Laki','Solo','1990-08-12','Lajang','Anak','Islam','SOLO',NULL,NULL,'SMU/Sederajat','Pelajar',0,NULL,0,NULL,NULL,0,1,0,NULL,0,0,NULL,0,NULL,'','0000-00-00',NULL,NULL);

/*Table structure for table `mwargameninggal` */

DROP TABLE IF EXISTS `mwargameninggal`;

CREATE TABLE `mwargameninggal` (
  `NIK` varchar(200) NOT NULL,
  `TanggalKematian` date NOT NULL,
  `SebabKematian` text,
  `CreatedBy` varchar(200) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `ModifiedBy` varchar(200) DEFAULT NULL,
  `ModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`NIK`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mwargameninggal` */

insert  into `mwargameninggal`(`NIK`,`TanggalKematian`,`SebabKematian`,`CreatedBy`,`CreatedDate`,`ModifiedBy`,`ModifiedDate`) values ('5555555','2018-08-22','TERSEDAK SANG PISANG','admin','2018-08-22 00:00:00',NULL,NULL);

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `RoleID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `RoleName` varchar(50) NOT NULL,
  PRIMARY KEY (`RoleID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `roles` */

insert  into `roles`(`RoleID`,`RoleName`) values (1,'Administrator'),(2,'User');

/*Table structure for table `settings` */

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `SettingID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SettingLabel` varchar(50) NOT NULL,
  `SettingName` varchar(50) NOT NULL,
  `SettingValue` text NOT NULL,
  PRIMARY KEY (`SettingID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `settings` */

/*Table structure for table `translation` */

DROP TABLE IF EXISTS `translation`;

CREATE TABLE `translation` (
  `TranslationID` int(11) NOT NULL AUTO_INCREMENT,
  `LanguageID` int(11) NOT NULL,
  `Word` varchar(200) NOT NULL,
  `Translation` varchar(200) NOT NULL,
  `Tags` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`TranslationID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `translation` */

/*Table structure for table `userinformation` */

DROP TABLE IF EXISTS `userinformation`;

CREATE TABLE `userinformation` (
  `UserName` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `CompanyID` varchar(200) DEFAULT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `IdentityNo` varchar(50) DEFAULT NULL,
  `BirthDate` date DEFAULT NULL,
  `ReligionID` int(10) DEFAULT NULL,
  `Gender` tinyint(1) DEFAULT NULL,
  `Address` text,
  `PhoneNumber` varchar(50) DEFAULT NULL,
  `EducationID` int(10) DEFAULT NULL,
  `UniversityName` varchar(50) DEFAULT NULL,
  `FacultyName` varchar(50) DEFAULT NULL,
  `MajorName` varchar(50) DEFAULT NULL,
  `IsGraduated` tinyint(1) NOT NULL DEFAULT '0',
  `GraduatedDate` date DEFAULT NULL,
  `YearOfExperience` int(10) DEFAULT NULL,
  `RecentPosition` varchar(250) DEFAULT NULL,
  `RecentSalary` double DEFAULT NULL,
  `ExpectedSalary` double DEFAULT NULL,
  `CVFilename` varchar(250) DEFAULT NULL,
  `ImageFilename` varchar(250) DEFAULT NULL,
  `RegisteredDate` date DEFAULT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `userinformation` */

insert  into `userinformation`(`UserName`,`Email`,`CompanyID`,`Name`,`IdentityNo`,`BirthDate`,`ReligionID`,`Gender`,`Address`,`PhoneNumber`,`EducationID`,`UniversityName`,`FacultyName`,`MajorName`,`IsGraduated`,`GraduatedDate`,`YearOfExperience`,`RecentPosition`,`RecentSalary`,`ExpectedSalary`,`CVFilename`,`ImageFilename`,`RegisteredDate`) values ('admin','admin@sidasma.humbanghasundutankab.go.id',NULL,'Administrator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-17'),('kader1','kader@gmail.com','5','Kader 1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-09-06'),('rirismanik','rirismanik17@humbanghasundutankab.go.id','1,3,5','RIRIS MANIK',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-18'),('yoelrolas','yoelrolas@gmail.com','1,4,5','YOEL ROLAS S',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-18');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `RoleID` int(10) unsigned NOT NULL,
  `IsSuspend` tinyint(1) unsigned NOT NULL,
  `LastLogin` datetime DEFAULT NULL,
  `LastLoginIP` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`UserName`,`Password`,`RoleID`,`IsSuspend`,`LastLogin`,`LastLoginIP`) values ('admin','e10adc3949ba59abbe56e057f20f883e',1,0,'2018-10-03 14:12:07','::1'),('kader1','e10adc3949ba59abbe56e057f20f883e',2,0,'2018-10-01 17:12:28','::1'),('rirismanik','bbfb3b97637d3caa18d4f73c6bf1b3b6',2,0,'2018-08-18 18:18:59','127.0.0.1'),('yoelrolas','bbfb3b97637d3caa18d4f73c6bf1b3b6',2,0,'2018-08-22 14:15:47','::1');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
